#ifndef _GEOMETRIC_FUNCTIONS_HPP_
#define _GEOMETRIC_FUNCTIONS_HPP_

#include <iostream>
#include <vector>
#include <glm/glm.hpp>

/* Center the model on screen, and bring it back to [-1; 1] */
void center_and_scale_model(std::vector<glm::vec3>& vertices);

/* Computes the normals of each triangle */
void compute_triangle_normals (	const std::vector<glm::vec3>& vertices,
				const std::vector<std::vector<unsigned short>>& indices,
				std::vector<glm::vec3> & triangle_normals);

/* Collects the neighboring vertices of each vertex, indexed according to the vertices in the array provided */
void collect_one_ring(	const std::vector<glm::vec3> & vertices,
			const std::vector<std::vector<unsigned short> > & triangles,
			std::vector<std::vector<unsigned short> > & one_ring);

/* Computes the valences of each ring, which is equal to the number of vertices in each one-ring */
void compute_vertex_valences(	const std::vector<std::vector<unsigned short>>& one_ring,
				std::vector<unsigned int> & valences,
				unsigned int &maxVal) ;

/* Shortcut to compute one-ring and valence together, as one step */
void collect_one_ring_and_compute_valence (	const std::vector<glm::vec3>& vertices,
						const std::vector<std::vector<unsigned short>>& vertex_indices,
						std::vector<unsigned int>& valences,
						unsigned int & maxVal,
						std::vector<std::vector<unsigned short> > & one_ring); 

/* Computes the normals per-vertex of each vertex */
void compute_smooth_vertex_normals (	unsigned int weight_type,
					const std::vector<glm::vec3> & vertices,
					const std::vector<unsigned short> & indices,
					const std::vector<std::vector<unsigned short>> & vertex_indices,
					std::vector<unsigned int>& vertex_valences,
					unsigned int & maxVal,
					std::vector<glm::vec3> & vertex_normals);

/* Computes the area of a given triangle */
double compute_triangle_area(const std::vector<glm::vec3>& vertices, const std::vector<unsigned short>& indices);

/* Computes the mean curvature direction using the laplacian operator */
void calc_uniform_mean_curvature(	const std::vector<glm::vec3>& vertices, 
					std::vector<glm::vec3>& vunicurvature_,
					const std::vector<unsigned short>& indices,
					const std::vector<std::vector<unsigned short>> & vertex_indices);

/* Compute curvature and   S  M  O  O  T  H    I  T    O  U  T */
void calc_uniform_smoothing(	size_t nb_iter,
				std::vector<glm::vec3>& vertices,
				const std::vector<unsigned short> &indices,
				const std::vector<std::vector<unsigned short> > &vertex_indices);

#endif // _GEOMETRIC_FUNCTIONS_HPP_
