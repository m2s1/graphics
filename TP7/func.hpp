#ifndef _func_hpp_pointset_
#define _func_hpp_pointset_

#include "src/Camera.h"
#include <GL/glut.h>
#include <string>
#include <vector>
#include <algorithm>

typedef enum available_kernel_types {
	SINGULAR,
	GAUSSIAN,
	WENDLAND
} kernel_t;

static	GLint		window;
static	Camera		camera;
static	unsigned int	SCREENWIDTH		= 640;
static	unsigned int	SCREENHEIGHT		= 480;
static	bool		mouseRotatePressed	= false;
static	bool		mouseMovePressed	= false;
static	bool		mouseZoomPressed	= false;
static	int		lastX			= 0;
static	int		lastY			= 0;
static	int		lastZoom		= 0;
static	bool		fullScreen		= false;

Vec3 	hermite_approximate_projection	(const Vec3& x, const Vec3& pi, const Vec3& ni);
void	loadPN				(const std::string& filename, std::vector<Vec3>& o_positions, std::vector<Vec3>& o_normals);
void	savePN				(const std::string& filename, std::vector<Vec3> const& o_positions, std::vector<Vec3> const& o_normals);
void	scaleAndCenter			(std::vector<Vec3>& io_positions);
void	applyRandomRigidTransformation	(std::vector<Vec3>& io_positions , std::vector<Vec3>& io_normals);
void	subsample			(std::vector<Vec3>& i_positions, std::vector<Vec3>& i_normals, float minimumAmount = 0.1f, float maximumAmount = 0.2f);

#endif // _func_hpp_pointset_
