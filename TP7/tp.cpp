#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <float.h>
#include "src/Vec3.h"
#include "src/jmkdtree.h"

#include "./func.hpp"

bool showmodel = true;

std::vector< Vec3 > positions;
std::vector< Vec3 > normals;

std::vector< Vec3 > positions2;
std::vector< Vec3 > normals2;

#define S_weight 1.0f

// I/O and some stuff (rigid transformation, subsample) {{{
// }}}

// Rendering {{{

// Light initialization {{{
void initLight () {
	GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
	GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
	GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

	glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
	glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
	glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
	glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
	glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
	glEnable (GL_LIGHT1);
	glEnable (GL_LIGHTING);
}
// }}}

// GL Environment initialization {{{
void init () {
	camera.resize (SCREENWIDTH, SCREENHEIGHT);
	initLight ();
	glCullFace (GL_BACK);
	glEnable (GL_CULL_FACE);
	glDepthFunc (GL_LESS);
	glEnable (GL_DEPTH_TEST);
	glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
	glEnable(GL_COLOR_MATERIAL);
}
// }}}

// Draw mesh {{{
void drawTriangleMesh( std::vector< Vec3 > const & i_positions , std::vector< unsigned int > const & i_triangles ) {
	if (showmodel) {
		glBegin(GL_TRIANGLES);
		for(unsigned int tIt = 0 ; tIt < i_triangles.size() / 3 ; ++tIt) {
			Vec3 p0 = i_positions[3*tIt];
			Vec3 p1 = i_positions[3*tIt+1];
			Vec3 p2 = i_positions[3*tIt+2];
			Vec3 n = Vec3::cross(p1-p0 , p2-p0);
			n.normalize();
			glNormal3f( n[0] , n[1] , n[2] );
			glVertex3f( p0[0] , p0[1] , p0[2] );
			glVertex3f( p1[0] , p1[1] , p1[2] );
			glVertex3f( p2[0] , p2[1] , p2[2] );
		}
		glEnd();
	}
}
// }}}

// Draw point cloud {{{
void drawPointSet( std::vector< Vec3 > const & i_positions , std::vector< Vec3 > const & i_normals ) {
	glBegin(GL_POINTS);
	for(unsigned int pIt = 0 ; pIt < i_positions.size() ; ++pIt) {
		glNormal3f( i_normals[pIt][0] , i_normals[pIt][1] , i_normals[pIt][2] );
		glVertex3f( i_positions[pIt][0] , i_positions[pIt][1] , i_positions[pIt][2] );
	}
	glEnd();
}
// }}}

// Draw environment {{{
void draw () {
	glPointSize(2); // for example...
	if(showmodel) {
		glColor3f(0.8,0.8,1);
		drawPointSet(positions , normals);
	}
	glColor3f(1,0.5,0.5);
	glPointSize(5.0);
	drawPointSet(positions2 , normals2);
}
// }}}

// OpenGL calls for drawing {{{
void display () {
	glLoadIdentity ();
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	camera.apply ();
	draw ();
	glFlush ();
	glutSwapBuffers ();
}
// }}}

// }}}

void idle () {
	glutPostRedisplay ();
}

// User keyboard input {{{
void key (unsigned char keyPressed, int x, int y) {
	switch (keyPressed) {
	case 'f':
		if (fullScreen == true) {
			glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
			fullScreen = false;
		} else {
			glutFullScreen ();
			fullScreen = true;
		}
		break;

	case 'w':
		GLint polygonMode[2];
		glGetIntegerv(GL_POLYGON_MODE, polygonMode);
		if(polygonMode[0] != GL_FILL)
			glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
		else
			glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
		break;

	case 'q':
		exit(EXIT_SUCCESS);
	case 's':
		showmodel = !showmodel;
		std::cout << " show model ? " << std::boolalpha << showmodel << std::endl;
		break;
	default:
		break;
	}
	idle ();
}
// }}}

// User mouse input {{{
void mouse (int button, int state, int x, int y) {
	if (state == GLUT_UP) {
		mouseMovePressed = false;
		mouseRotatePressed = false;
		mouseZoomPressed = false;
	} else {
		if (button == GLUT_LEFT_BUTTON) {
			camera.beginRotate (x, y);
			mouseMovePressed = false;
			mouseRotatePressed = true;
			mouseZoomPressed = false;
		} else if (button == GLUT_RIGHT_BUTTON) {
			lastX = x;
			lastY = y;
			mouseMovePressed = true;
			mouseRotatePressed = false;
			mouseZoomPressed = false;
		} else if (button == GLUT_MIDDLE_BUTTON) {
			if (mouseZoomPressed == false) {
				lastZoom = y;
				mouseMovePressed = false;
				mouseRotatePressed = false;
				mouseZoomPressed = true;
			}
		}
	}
	idle ();
}
// }}}

// Camera movement from mouse input {{{
void motion (int x, int y) {
	if (mouseRotatePressed == true) {
		camera.rotate (x, y);
	}
	else if (mouseMovePressed == true) {
		camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
		lastX = x;
		lastY = y;
	}
	else if (mouseZoomPressed == true) {
		camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
		lastZoom = y;
	}
}
// }}}

// Window resize {{{
void reshape(int w, int h) {
	camera.resize (w, h);
}
// }}}

Vec3 project_on_line(const Vec3& x, const Vec3& c, const Vec3& n) {
	return c - ((Vec3::dot(x - c, n) / n.squareLength()) * n);
}

void HPSS(Vec3 inputPoint, Vec3 & outputPoint, Vec3 & outputNormal, std::vector<Vec3>const& positions, std::vector<Vec3>const& normals,BasicANNkdTree const& kdtree, kernel_t, float radius , unsigned int nbIterations= 10, unsigned int knn = 20 );

void APSS(Vec3 inputPoint, Vec3 & outputPoint, Vec3 & outputNormal, std::vector<Vec3>const& positions, std::vector<Vec3>const& normals,BasicANNkdTree const& kdtree, kernel_t, float radius , unsigned int nbIterations= 10, unsigned int knn = 20 );

int main (int argc, char ** argv) {
	// init opengl {{{
	if (argc > 2) {
		exit (EXIT_FAILURE);
	}
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
	window = glutCreateWindow ("OpenGL : Point Processing");

	init ();
	glutIdleFunc (idle);
	glutDisplayFunc (display);
	glutKeyboardFunc (key);
	glutReshapeFunc (reshape);
	glutMotionFunc (motion);
	glutMouseFunc (mouse);
	key ('?', 0, 0);
	// }}}

	// Load a first pointset, and build a kd-tree:
	loadPN("pointsets/face.pn" , positions , normals);

	BasicANNkdTree kdtree;
	kdtree.build<Vec3>(positions);

	// Create a second pointset that is artificial, and project it on pointset1 using MLS techniques:
	positions2.resize( 100000 );
	normals2.resize(positions2.size());
	for( unsigned int pIt = 0 ; pIt < positions2.size() ; ++pIt ) {
		positions2[pIt] = Vec3(
			-2.0 + 4.0 * (double)(rand())/(double)(RAND_MAX),
			-2.0 + 4.0 * (double)(rand())/(double)(RAND_MAX),
			-2.0 + 4.0 * (double)(rand())/(double)(RAND_MAX)
		);
		//positions2[pIt].normalize();
		//positions2[pIt] = 0.6 * positions2[pIt];
	}

	double alpha = 0.01;
	/* Inject noise on the base model before projection : */
/*	for (size_t i = 0; i < positions.size(); ++i) {
		Vec3& p = positions[i];
		double rand_noise_addition = -(2.0*alpha) + 2.0 * (alpha * (double)(rand())/(double)(RAND_MAX));
		p += rand_noise_addition * normals[i];
	}
*/
	unsigned int nbiter = 4;
	// PROJECT USING MLS (HPSS and APSS):
	for (size_t i = 0; i < positions2.size(); ++i) {
		const Vec3 v = Vec3(positions2[i]);
		HPSS(v, positions2[i], normals2[i], positions, normals, kdtree, kernel_t::GAUSSIAN, 0.00001, nbiter, 20);
	}

	glutMainLoop ();
	return EXIT_SUCCESS;
}

/* HPSS {{{ */
void HPSS( Vec3 inputPoint, Vec3 & outputPoint, Vec3 & outputNormal, std::vector<Vec3>const& positions, std::vector<Vec3>const& normals, BasicANNkdTree const& kdtree, kernel_t k_type, float radius, unsigned int nbIterations, unsigned int knn) {
	// Iterate for 'nbIterations' projections
	for (size_t it = 0; it < static_cast<size_t>(nbIterations); ++it) {
		// If the output point/normal wasn't at 0, put it there.
		outputPoint = Vec3(.0f,.0f,.0f);
		// We need to get the nearest points in the kd-tree :
		ANNidxArray idxs = new ANNidx[knn];
		ANNdistArray dists = new ANNdist[knn];
		kdtree.knearest(inputPoint, knn, idxs, dists);
		// Get furthest point : (ignoring the radius parameter in argument)
		ANNidx furthest_point = idxs[knn-1];
		ANNdist furthest_point_dist = dists[knn-1];
		Vec3 c;
		Vec3 n;
		// Compute weights and hermite nearest projections {{{
		double sum = .0;
		double weight = .0;
		for (size_t i = 0; i < knn; ++i) {
			double r = dists[i];
			switch (k_type) {
				case kernel_t::SINGULAR : { weight = (std::pow((furthest_point_dist / r), S_weight)); break; }
				case kernel_t::GAUSSIAN : { weight = ( std::pow( (1.0-(r/furthest_point_dist)), 4 ) * ( 1.0 + 4.0 * r/furthest_point_dist ) ); break; }
				case kernel_t::WENDLAND : { weight = ( std::exp( (-std::pow(r,2.0)) / (std::pow(furthest_point_dist, 2.0)) ) ); break; }
			}
			c += weight * hermite_approximate_projection(inputPoint, positions[i], normals[i]);
			n += weight * normals[i];
			sum += weight;
		}
		// }}}
		c /= sum;
		n /= sum;
		outputNormal = n;
		outputPoint = project_on_line(inputPoint, c, outputNormal);
		n.normalize();
		inputPoint = outputPoint;
		delete[] idxs;
		delete[] dists;
	}
}
/* }}} */

/* APSS {{{ */
void APSS(
	Vec3 inputPoint, Vec3 & outputPoint, Vec3 & outputNormal,
	std::vector<Vec3>const& positions, std::vector<Vec3>const& normals,
	BasicANNkdTree const& kdtree, kernel_t k_type, float radius,
	unsigned int nbIterations, unsigned int knn
) {
	// do all iterations :
	for (size_t i = 0; i < nbIterations; ++i) {
		ANNidxArray idxs = new ANNidx[knn];
		ANNdistArray dists = new ANNdist[knn];
		outputPoint = Vec3(.0,.0,.0);
		outputNormal = Vec3(.0,.0,.0);
		// get nearest points
		kdtree.knearest(inputPoint, knn, idxs, dists);
		// compute weights :
		std::vector<double> weights;
		std::vector<double> norm_weights;

		ANNdist max=.0;
		for (size_t i = 0; i < knn; ++i) {
			dists[i] = std::sqrt(dists[i]);
			if (dists[i] > max) {
				max = dists[i];
			}
		}

		double h = max;
		double sum = .0;
		for (size_t i = 0; i < knn; ++i) {
			double r = dists[i];
			switch (k_type) {
				case kernel_t::SINGULAR : {
					weights.push_back(std::pow((h / dists[i]), S_weight));
					break;
				}
				case kernel_t::GAUSSIAN : {
					weights.push_back( std::pow( (1.0-(r/h)), 4 ) * ( 1.0 + 4.0 * r/h ) );
					break;
				}
				case kernel_t::WENDLAND : {
					weights.push_back( std::exp( (-std::pow(r,2.0)) / (std::pow(h, 2.0)) ) );
					break;
				}
			}
			sum+= weights.back();
		}
		// normalize weights
		for (size_t i = 0; i < knn; ++i) {
			norm_weights.push_back(weights[i] / sum);
		}

		// compute sphere equation, and get projection point
		// compute u4 {{{
		double u4 = .0;
		double wipini = .0;
		double wipipi = .0;
		double wjpipi = .0;
		Vec3 wipi = Vec3(.0,.0,.0);
		Vec3 wjpi = Vec3(.0,.0,.0);
		Vec3 wjni = Vec3(.0,.0,.0);
		Vec3 wini = Vec3(.0,.0,.0);
		for (size_t i = 0; i < knn; ++i) {
			wipini += weights[i] * Vec3::dot(positions[idxs[i]], normals[idxs[i]]);
			wipipi += weights[i] * Vec3::dot(positions[idxs[i]], positions[idxs[i]]);
			wjpipi += norm_weights[i] * Vec3::dot(positions[idxs[i]], positions[idxs[i]]);
			wipi += weights[i] * positions[idxs[i]];
			wini += weights[i] * normals[idxs[i]];
			wjpi += norm_weights[i] * positions[idxs[i]];
			wjni += norm_weights[i] * positions[idxs[i]];
		}
		u4 = .5 * ( (wipini - Vec3::dot(wjpi, wini)) / (wipipi - Vec3::dot(wipi, wini)) );
		// }}}

		// compute plane normal {{{
		Vec3 pnormal = wjni - 2.0 * u4 * wjpi;
		pnormal.normalize();
		// }}}

		double u0 = Vec3::dot(pnormal, wjpi) - u4 * wjpipi;
	}
}
/* }}} */

// vim:foldmethod=marker:foldmarker={{{,}}}:tabstop=8:
