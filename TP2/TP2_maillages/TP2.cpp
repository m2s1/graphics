// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <algorithm>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "../common/shader.hpp"
#include "../common/texture.hpp"
#include "../common/objloader.hpp"
#include "../common/vboindexer.hpp"

// Compute triangle normals {{{
void compute_triangle_normals (const std::vector<glm::vec3>& vertices,
				const std::vector<std::vector<unsigned short>>& indices,
				std::vector<glm::vec3> & triangle_normals){


	triangle_normals.resize(indices.size()); // should resize to the good number of triangles in the mesh

	// for all triangles :
	for (size_t i = 0; i < indices.size(); ++i) {
		// get one vector :
		glm::vec3 v1 = vertices[indices[i][1]] - vertices[indices[i][0]];
		glm::vec3 v2 = vertices[indices[i][2]] - vertices[indices[i][0]];
	//	if (glm::dot(v1, v2) < 0) {
	//		v2 = -v2;
	//	}
		triangle_normals[i] = glm::cross(v1, v2);
	}

}
// }}}

// Collect the 1-ring of all vertices in the polygon soup {{{
void collect_one_ring (const std::vector<glm::vec3>& vertices,
		const std::vector<unsigned short> & indices,
		const std::vector<std::vector<unsigned short>>& vertex_indices,
		std::vector<std::vector<unsigned short>> & one_ring) { // to rule them all

	// we shall go through the list together ~~
	one_ring.resize(vertices.size());
	// initial cleanup of the ring, to be sure :
	for (unsigned int i = 0; i < one_ring.size(); ++i) { one_ring[i].clear(); }

	// we now will go through the concantenated list together
	for (size_t i = 0; i < indices.size(); ++i) {
		// take the current face index :
		unsigned short curVert = indices[i];
		// it will serve as the basis for finding all neighboring triangles :
		for (size_t j = i+1; j < indices.size(); ++j) {
			if (indices[j] == curVert) {
				// we found a triangle also containing the vertex :
				unsigned int remainder = j%3;
				unsigned short triangleConcerned = (j - remainder) / 3;
				if (std::find(one_ring[curVert].begin(), one_ring[curVert].end(), triangleConcerned) == one_ring[curVert].end()) {
					one_ring[curVert].push_back(triangleConcerned);
				}
			}
		}
	}

	return;
}
// }}}

// Valence computation {{{
void compute_vertex_valences (const std::vector<std::vector<unsigned short>>& one_ring, std::vector<unsigned int> & valences, unsigned int &maxVal) {
	valences.resize(one_ring.size());
	unsigned int max = 0;
	for (size_t i = 0; i < one_ring.size(); ++i) {
		valences[i] = one_ring[i].size();
		if (valences[i] > max) {
			max = valences[i];
		}
	}
	std::cout << "Max valence in the mesh : " << max << std::endl;
	maxVal = max;
}
// }}}

// Shortcut to compute valence and 1-ring together {{{
void collect_one_ring_and_compute_valence (const std::vector<glm::vec3>& vertices,
					const std::vector<unsigned short> & indices,
					const std::vector<std::vector<unsigned short>>& vertex_indices,
					std::vector<unsigned int>& valences,
					unsigned int & maxVal,
					std::vector<std::vector<unsigned short> > & one_ring) { // to rule them all
	collect_one_ring(vertices, indices, vertex_indices, one_ring);
	compute_vertex_valences(one_ring,valences, maxVal);
}
// }}}

// Compute area of triangle {{{
double compute_triangle_area(const std::vector<glm::vec3>& vertices, const std::vector<unsigned short>& indices) {
	// using Heron's formula
	glm::vec3 A = vertices[indices[1]] - vertices[indices[0]];	double a = A.length();
	glm::vec3 B = vertices[indices[2]] - vertices[indices[1]];	double b = B.length();
	glm::vec3 C = vertices[indices[0]] - vertices[indices[2]];	double c = C.length();
	double p = (a+b+c)/2.0; // perimeter

	return glm::sqrt(p * (p-a) * (p-b) * (p-c));
}
// }}}

// Compute smooth normals {{{
void compute_smooth_vertex_normals (
		unsigned int weight_type,
		const std::vector<glm::vec3> & vertices,
		const std::vector<unsigned short> & indices,
		const std::vector<std::vector<unsigned short>> & vertex_indices,
		std::vector<unsigned int>& vertex_valences,
		unsigned int & maxVal,
		std::vector<glm::vec3> & vertex_normals) {
	float w = 1.0; // default: uniform weights

	// compute normals per vertices

	std::vector<std::vector<unsigned short>> one_ring ;
	// ask for the 1-ring of each vertices, to get all neighboring vertices
	collect_one_ring_and_compute_valence(vertices, indices, vertex_indices, vertex_valences, maxVal, one_ring);
	std::vector<glm::vec3> triangle_normals;
	compute_triangle_normals(vertices, vertex_indices, triangle_normals);

	// clear the vector of normals :
	vertex_normals.clear();
	vertex_normals.resize(vertices.size());

	std::vector<std::vector<double>> normalWeights;
	normalWeights.resize(vertices.size());

	if (weight_type == 1) { // area weight
		// weight normals according to the area of the face
		for (unsigned int i = 0; i < one_ring.size(); ++i) {
			std::vector<double> areas = std::vector<double>();
			double totalarea = 0.0;
			// for each vertex get the area of the triangles :
			for(size_t j = 0; j < one_ring[i].size(); ++j ){
				// get the area of each triangle :
				areas.push_back(compute_triangle_area(vertices, vertex_indices[one_ring[i][j]]));
				// add it to the total area :
				totalarea += areas[j];
			}
			for (unsigned int j = 0; j < one_ring[i].size(); ++j) {
				areas[j] /= totalarea;
			}
			normalWeights[i] = std::vector<double>(areas);
		}
	} else if (weight_type == 2) { // angle weight

	} else {
		for (unsigned int i = 0; i < one_ring.size(); ++i) {
			normalWeights[i].resize(one_ring[i].size());
			std::fill(normalWeights[i].begin(), normalWeights[i].end(), 1.0/((double)one_ring[i].size()));
		}
	}

	for (unsigned int i = 0; i < vertices.size(); ++i) {
		glm::vec3 finalNormal = glm::vec3(0.0);
		for (unsigned int j = 0; j < one_ring[i].size(); ++j) {
			glm::vec3 curNorm = glm::vec3(triangle_normals[one_ring[i][j]]);
			curNorm *= normalWeights[i][j];
			finalNormal+=curNorm;
		}
		vertex_normals[i] = finalNormal;
	}
}
// }}}

double orbit = 0.0;
double height = M_PI/4.0;
		glm::vec3 lightPos = glm::vec3(0,75,-45); // for the armadillo model, light ain't that bright ...
double lx = .0, ly = 75.0, lz=-45.0;

int main( void )
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 1024, 768, "OpenGL : TP2 - Maillages", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	// Hide the mouse and enable unlimited mouvement
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set the mouse at the center of the screen
	glfwPollEvents();
	glfwSetCursorPos(window, 1024/2, 768/2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "vertex_shader.glsl", "fragment_shader.glsl" );

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");

	// Load the texture
	GLuint Texture = loadDDS("uvmap.DDS");

	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID  = glGetUniformLocation(programID, "myTextureSampler");

	std::vector<unsigned short> indices; //Triangles concaténés dans une liste
	std::vector<std::vector<unsigned short> > triangles;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;
	std::vector<unsigned int> vertex_valences;
	unsigned int maxValence;

	//Chargement du fichier de maillage
	std::string filename("arma1.off");
	loadOFF(filename, indexed_vertices, indices, triangles );
	indexed_uvs.resize(indexed_vertices.size(), glm::vec2(1.)); //List vide de UV

	//Calculer les normales par sommet, et la valence
	indexed_normals.resize(indexed_vertices.size(), glm::vec3(1.));
	compute_smooth_vertex_normals(0, indexed_vertices, indices, triangles, vertex_valences, maxValence, indexed_normals);

	// Load it into a VBO

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

	GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);

	GLuint normalbuffer;
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);

	// Generate a buffer for a vertex's valence
	GLuint valenceBuffer;
	glGenBuffers(1, &valenceBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, valenceBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertex_valences.size() * sizeof(unsigned int), &vertex_valences[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0] , GL_STATIC_DRAW);

	// Get a handle for our "LightPosition" uniform
	glUseProgram(programID);
	GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");

	// For speed computation
	double lastTime = glfwGetTime();
	int nbFrames = 0;

	do{

		// Get keypresses :
		if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
			orbit += .01;
		} 
		if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS) {
			orbit -= .01;
		}
		if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS) {
			height -= .01;
		}
		if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
			height += .01;
		}
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			ly += .1;
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			ly -= .1;
		}
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			lx += .1;
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			lx -= .1;
		}
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
			lz += .1;
		}
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
			lz -= .1;
		}
		if (height >= M_PI/2) { height = M_PI/2-.01; }
		if (height <=-M_PI/2) { height =-M_PI/2+.01; }

		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if ( currentTime - lastTime >= 1.0 ){ // If last prinf() was more than 1sec ago
			// printf and reset
			printf("%f ms/frame\n", 1000.0/double(nbFrames));
			nbFrames = 0;
			lastTime += 1.0;
		}

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		// Projection matrix : 45 Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
		glm::mat4 ProjectionMatrix = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 200.0f);
		// Camera matrix
		glm::mat4 ViewMatrix       = glm::lookAt(
						glm::vec3(100*cos(orbit),100*cos(height),100*sin(orbit)), // Camera is at (4,3,3), in World Space 			// For suzanne : glm::vec3(0,0,3)
						glm::vec3(0,0,0), // and looks at the origin
						glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
						);
		// Model matrix : an identity matrix (model will be at the origin)
		glm::mat4 ModelMatrix      = glm::mat4(1.0f);
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		// Send our transformation to the currently bound shader,
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);

		glm::vec3 lightPos = glm::vec3(lx,ly,lz); // for arma, light ain't that bright ...
		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture);
		// Set our "myTextureSampler" sampler to use Texture Unit 0
		glUniform1i(TextureID, 0);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
				0,                  // attribute
				3,                  // size
				GL_FLOAT,           // type
				GL_FALSE,           // normalized?
				0,                  // stride
				(void*)0            // array buffer offset
		);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
				1,                                // attribute
				2,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
		);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glVertexAttribPointer(
				2,                                // attribute
				3,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
		);

		// 4th attribute buffer : vertex valences
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, valenceBuffer);
		glVertexAttribPointer(
				3,                                // attribute
				1,                                // size
				GL_UNSIGNED_SHORT,                // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
		);
		glUniform1ui(glGetUniformLocation(programID, "maxValence"), maxValence);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			indices.size(),    // count
			GL_UNSIGNED_SHORT,   // type
			(void*)0           // element array buffer offset
		);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0 );

	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteBuffers(1, &normalbuffer);
	glDeleteBuffers(1, &elementbuffer);
	glDeleteProgram(programID);
	glDeleteTextures(1, &Texture);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

