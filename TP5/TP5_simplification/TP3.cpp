// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <algorithm>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "../common/shader.hpp"
#include "../common/texture.hpp"
#include "../common/objloader.hpp"
#include "../common/vboindexer.hpp"
#include "geometric_functions.hpp"

//#define ENABLE_WIREFRAME

// Collect the 1-ring of all vertices in the polygon soup {{{
void collect_one_ring (const std::vector<glm::vec3> & vertices,
			const std::vector<std::vector<unsigned short> > & triangles,
			std::vector<std::vector<unsigned short> > & one_ring) {

	one_ring.resize (vertices.size ());
	// for all triangles
	for (unsigned int i = 0; i < triangles.size (); i++) {
		const std::vector<unsigned short> & ti = triangles[i];
		// for all the vertices in this triangle
		for (unsigned int j = 0; j < 3; j++) {
			unsigned short vj = ti[j];
			// check all other triangle vertices
			for (unsigned int k = 1; k < 3; k++) {
				unsigned int vk = ti[(j+k)%3];
				// if t[k] not into the 1ring of t[j], add it :
				if (std::find (one_ring[vj].begin (), one_ring[vj].end (), vk) == one_ring[vj].end ())
					one_ring[vj].push_back (vk);
				}
		}
	}
}
// }}}

double orbit = 0.0;
double height = M_PI/4.0;
glm::vec3 lightPos = glm::vec3(0,75,-45); // for arma, light ain't that bright ...
double lx = .0, ly = 3.0, lz=.0;
bool key_pressed = false;


int main( void )
{
	// Initialise GLFW and openGL {{{
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 1024, 768, "OpenGL : TP2 - Maillages", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	// Hide the mouse and enable unlimited mouvement
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set the mouse at the center of the screen
	glfwPollEvents();
	glfwSetCursorPos(window, 1024/2, 768/2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	#ifdef ENABLE_WIREFRAME
	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	#endif

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "vertex_shader.glsl", "fragment_shader.glsl" );

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");

	// Load the texture
	GLuint Texture = loadDDS("uvmap.DDS");

	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID  = glGetUniformLocation(programID, "myTextureSampler");

	// }}}

	std::vector<unsigned short> indices; //Triangles concaténés dans une liste
	std::vector<glm::vec3> vunicurvature_;
	std::vector<std::vector<unsigned short> > triangles;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;
	std::vector<unsigned int> vertex_valences;
	std::vector<float> valences;
	unsigned int maxValence;

	std::cerr << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << "Les normales ne marchent pas (faute au\none_ring qui ne faisait pas le bon\ncalcul avant).\nLe modèle n'apparait pas : press R\nTourner le modèle : press IJKL\nBouger la lumière : press ZQSD\nAppliquer lissage : press F" << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << std::endl;

	//Chargement du fichier de maillage
	//std::string filename("bunny_simplified.off");
	std::string filename("./arma1.off");
	//std::string filename("./bunny.off");
	loadOFF(filename, indexed_vertices, indices, triangles );
	indexed_uvs.resize(indexed_vertices.size(), glm::vec2(1.)); //List vide de UV

	//Calculer les normales par sommet, et la valence
	indexed_normals.resize(indexed_vertices.size(), glm::vec3(1.));
	compute_smooth_vertex_normals(0, indexed_vertices, indices, triangles, vertex_valences, maxValence, indexed_normals);
	for (unsigned int vv : vertex_valences) {
		valences.push_back(static_cast<float>(vv));
	}

	// Load it into a VBO {{{

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

	GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);

	GLuint normalbuffer;
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);

	// Generate a buffer for a vertex's valence
	GLuint valenceBuffer;
	glGenBuffers(1, &valenceBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, valenceBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertex_valences.size() * sizeof(float), &valences[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0] , GL_STATIC_DRAW);

	// }}}

	// Get a handle for our "LightPosition" uniform
	glUseProgram(programID);
	GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");

	// For speed computation
	double lastTime = glfwGetTime();
	int nbFrames = 0;
	float orbit_size = 1.1f;

	do{

		// Get user input and measure time {{{
		if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
			orbit += .01;
		}
		if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS) {
			orbit -= .01;
		}
		if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS) {
			height -= .01;
		}
		if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
			height += .01;
		}
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			ly += .1;
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			ly -= .1;
		}
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			lx += .1;
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			lx -= .1;
		}
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
			lz += .1;
		}
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
			lz -= .1;
		}
		if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
			orbit_size -= .01f;
			if (orbit_size < .1f) { orbit_size = .1f; }
		}
		if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
			orbit_size += .01f;
		}
		if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS) {
			calc_uniform_smoothing(1, indexed_vertices, indices, triangles);
			compute_smooth_vertex_normals(0, indexed_vertices, indices, triangles, vertex_valences, maxValence, indexed_normals);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);
		}
		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS && key_pressed == false) {
			center_and_scale_model(indexed_vertices);
			compute_smooth_vertex_normals(0, indexed_vertices, indices, triangles, vertex_valences, maxValence, indexed_normals);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);
			std::cout << "Resizing" << std::endl;
			key_pressed = true;
		}
		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_RELEASE) {
			key_pressed = false;
		}
		if (height >= M_PI/2.0) { height = M_PI/2.0-.01; }
		if (height <=-M_PI/2.0) { height =-M_PI/2.0+.01; }

		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if ( currentTime - lastTime >= 1.0 ){ // If last prinf() was more than 1sec ago
			// printf and reset
			printf("%f ms/frame\n", 1000.0/double(nbFrames));
			nbFrames = 0;
			lastTime += 1.0;
		}

		// }}}

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		// Compute and send matrices to shader {{{

		// Projection matrix : 45 Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
		glm::mat4 ProjectionMatrix = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.01f, 10.f);
		// Camera matrix
		glm::mat4 ViewMatrix       = glm::lookAt(
						glm::vec3(orbit_size*cos(orbit),orbit_size*sin(height),orbit_size*sin(orbit)), // Camera is at (4,3,3), in World Space 			// For suzanne : glm::vec3(0,0,3)
						glm::vec3(0,0,0), // and looks at the origin
						glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
						);
		// Model matrix : an identity matrix (model will be at the origin)
		glm::mat4 ModelMatrix      = glm::mat4(1.0f);
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		// Send our transformation to the currently bound shader,
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);

		glm::vec3 lightPos = glm::vec3(lx,ly,lz); // for arma, light ain't that bright ...
		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

		// }}}

		// Set up arrays to be used by the shaders and draw them {{{

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture);
		// Set our "myTextureSampler" sampler to use Texture Unit 0
		glUniform1i(TextureID, 0);

		// 1rst attribute buffer : vertices {{{
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
				0,                  // attribute
				3,                  // size
				GL_FLOAT,           // type
				GL_FALSE,           // normalized?
				0,                  // stride
				(void*)0            // array buffer offset
		);
		// }}}

		// 2nd attribute buffer : UVs {{{
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
				1,                                // attribute
				2,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
		);
		// }}}

		// 3rd attribute buffer : normals {{{
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glVertexAttribPointer(
				2,                                // attribute
				3,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
		);
		// }}}

		// 4th attribute buffer : vertex valences {{{
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, valenceBuffer);
		glVertexAttribPointer(
				3,                                // attribute
				1,                                // size
				GL_FLOAT,                // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
		);
		// }}}

		glUniform1ui(glGetUniformLocation(programID, "maxValence"), maxValence);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		// Draw the triangles {{{
		glDrawElements(
			GL_TRIANGLES,      // mode
			indices.size(),    // count
			GL_UNSIGNED_SHORT,   // type
			(void*)0           // element array buffer offset
		);
		// }}}

		// }}}

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0 );

	// Cleanup VBO and shader {{{
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteBuffers(1, &normalbuffer);
	glDeleteBuffers(1, &elementbuffer);
	glDeleteProgram(programID);
	glDeleteTextures(1, &Texture);
	glDeleteVertexArrays(1, &VertexArrayID);
	// }}}

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

