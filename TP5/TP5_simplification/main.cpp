// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <algorithm>
#include <tuple>
#include <iomanip>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "../common/shader.hpp"
#include "../common/texture.hpp"
#include "../common/objloader.hpp"
#include "../common/vboindexer.hpp"
#include "geometric_functions.hpp"
#include "model.hpp"

double orbit = 0.0;
double height = M_PI/4.0;
glm::vec3 lightPos = glm::vec3(0,75,-45); // for arma, light ain't that bright ...
double lx = .0, ly = 3.0, lz=.0;
bool key_pressed = false;
bool wireframe_enabled = true;
bool key_wireframe_pressed = false;
bool unit_cube = false;
bool key_unit_cube_pressed = false;
bool show_normals = false;
bool key_normals_pressed = false;
bool simplify_asked = false;
bool simplify_key_press = false;
bool step_1 = false;
bool step_2 = false;
bool step_3 = false;
bool fps_key = false;
double frame_time = std::nan("");

int main( int argc, char* argv[] ) {

	// Initialise GLFW and openGL {{{
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 1024, 768, "OpenGL : TP2 - Maillages", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	// Hide the mouse and enable unlimited mouvement
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set the mouse at the center of the screen
	glfwPollEvents();
	glfwSetCursorPos(window, 1024/2, 768/2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "vertex_shader.glsl", "fragment_shader.glsl" );

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");
	GLuint max_valence_shader_field = glGetUniformLocation(programID, "max_valence");

	// Load the texture
	GLuint Texture = loadDDS("uvmap.DDS");

	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID  = glGetUniformLocation(programID, "myTextureSampler");

	// }}}

	std::cerr << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << "======================================" << std::endl;
/*	std::cerr << "Les normales ne marchent pas (faute au\n";
	std::cerr << "one_ring qui ne faisait pas le bon\n";
	std::cerr << "calcul avant).\n"; */
	std::cerr << "Le modèle n'apparait pas : press R\n";
	std::cerr << "Tourner le modèle : press IJKL\n";
	std::cerr << "Bouger la lumière : press ZQSD\n";
	std::cerr << "======================================" << std::endl;
	std::cerr << "F1 : Wireframe Mode" << std::endl;
	std::cerr << "F3 : Only normals Mode" << std::endl;
	std::cerr << "F4 : Show frame time" << std::endl;
	std::cerr << "F5 : Apply simplification (res=16)" << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << "======================================" << std::endl;
	std::cerr << std::endl;

	std::string pathname = "./bunny_simplified.off";
	if (argc > 1) {
		pathname = std::string(argv[1]);
	}
	std::cout << "Requested model : " << pathname << std::endl;
	model object_model = model(pathname);
	object_model.compute_smooth_vertex_normals(0);
	object_model.compute_vertex_valences();

	/* load into VBO {{{ */
	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, object_model.get_vertex_count() * sizeof(glm::vec3), &object_model.get_first_vertex(), GL_DYNAMIC_DRAW);

	GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, object_model.get_uv_count() * sizeof(glm::vec2), &object_model.get_first_uv(), GL_STATIC_DRAW);

	GLuint normalbuffer;
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, object_model.get_normal_count() * sizeof(glm::vec3), &object_model.get_first_normal(), GL_STATIC_DRAW);

	// Generate a buffer for a vertex's valence
	GLuint valenceBuffer;
	glGenBuffers(1, &valenceBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, valenceBuffer);
	glBufferData(GL_ARRAY_BUFFER, object_model.get_triangle_count() * sizeof(float), &object_model.get_first_triangle(), GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, object_model.get_triangle_count() * 3 * sizeof(unsigned short), &object_model.get_first_index(), GL_DYNAMIC_DRAW);
	/* }}} */

	// Get a handle for our "LightPosition" uniform
	glUseProgram(programID);
	GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");

	// For speed computation
	double lastTime = glfwGetTime();
	int nbFrames = 0;
	float orbit_size = 1.9f;
	float s;
	glm::vec3 lb, ub; // lb = lower bound, ub = upper bound
	std::tie(lb,ub) = object_model.get_square_boundary_box();
	s = std::sqrt(3.f) / glm::length(ub-lb); // std::sqrt(3.f) / glm::length(ub);
	glm::vec3 diag = ub-lb;

	do{

		// Get user input and measure time {{{
		// Get position around model {{{
		if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
			orbit += .01;
		}
		if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS) {
			orbit -= .01;
		}
		if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS) {
			height -= .01;
		}
		if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
			height += .01;
		}
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			ly += .1;
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			ly -= .1;
		}
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			lx += .1;
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			lx -= .1;
		}
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
			lz += .1;
		}
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
			lz -= .1;
		}
		if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
			orbit_size -= .01f;
			if (orbit_size < .1f) { orbit_size = .1f; }
		}
		if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
			orbit_size += .01f;
		}
		// }}}
		// Apply centering {{{
		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS && key_pressed == false) {
			object_model.center_and_scale_model();
			object_model.compute_smooth_vertex_normals(0);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, object_model.get_vertex_count() * sizeof(glm::vec3), &object_model.get_first_vertex(), GL_DYNAMIC_DRAW);
			std::cout << "Resizing" << std::endl;
			key_pressed = true;
		}
		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_RELEASE) {
			key_pressed = false;
		}
		// }}}
		// Show frame time {{{
		if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_PRESS && fps_key == false) {
			fps_key = true;
			std::cout << std::setw(8) << std::setprecision(8) << frame_time << " ms/frame" << std::endl;
		}
		if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_RELEASE) {
			fps_key = false;
		}
		// }}}
		// Wireframe mode {{{
		if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS && key_wireframe_pressed == false) {
			key_wireframe_pressed = true;
			if (wireframe_enabled) {
				glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
				wireframe_enabled = false;
			} else {
				glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
				wireframe_enabled = true;
			}
		}
		if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_RELEASE) {
			key_wireframe_pressed = false;
		}
		// }}}
		// Show unit cube {{{
		if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS && key_unit_cube_pressed == false) {
			key_unit_cube_pressed = true;
			std::cout << "Toggled unit cube" << std::endl;
			unit_cube = !unit_cube;
		}
		if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_RELEASE) {
			key_unit_cube_pressed = false;
		}
		// }}}
		// Show normals {{{
		if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_PRESS && key_normals_pressed == false) {
			key_normals_pressed = true;
			show_normals = !show_normals;
			if (show_normals) {
				glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
				glBufferData(
					GL_ARRAY_BUFFER,
					object_model.get_vertex_count() * 2 * sizeof(glm::vec3),
					&object_model.get_vertices_and_normals()[0],
					GL_DYNAMIC_DRAW
				);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
				glBufferData(
					GL_ELEMENT_ARRAY_BUFFER, 
					(object_model.get_vertex_count() * 2) * sizeof(unsigned short),
					&object_model.get_elements_vertices_normals()[0],
					GL_DYNAMIC_DRAW
				);
			} else {
				// Put back original data
				glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
				glBufferData(GL_ARRAY_BUFFER, object_model.get_vertex_count() * sizeof(glm::vec3), &object_model.get_first_vertex(), GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, (object_model.get_triangle_count() * 3) * sizeof(unsigned short), &object_model.get_first_index(), GL_DYNAMIC_DRAW);
			}
		}
		if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_RELEASE) {
			key_normals_pressed = false;
		}
		// }}}
		// Simplify the model {{{
		if (glfwGetKey(window, GLFW_KEY_F5) == GLFW_PRESS && simplify_key_press == false) {
			simplify_key_press = true;
			std::cerr << "Model simplification requested." << std::endl;
			object_model.simplify(32);
			std::cerr << "Model simplification done." << std::endl;
			object_model.compute_vertex_valences();
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, object_model.get_vertex_count() * sizeof(glm::vec3), &object_model.get_first_vertex(), GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
			glBufferData(GL_ARRAY_BUFFER, object_model.get_vertex_count() * sizeof(glm::vec3), &object_model.get_first_normal(), GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, valenceBuffer);
			glBufferData(GL_ARRAY_BUFFER, object_model.get_vertex_count() * sizeof(glm::vec3), &object_model.get_first_valence(), GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, object_model.get_triangle_count() * 3 * sizeof(unsigned short), &object_model.get_first_index(), GL_DYNAMIC_DRAW);
		}
		if (glfwGetKey(window, GLFW_KEY_F5) == GLFW_RELEASE) {
			simplify_key_press = false;
		}
		// }}}
		if (height >= M_PI/2.0) { height = M_PI/2.0-.01; }
		if (height <=-M_PI/2.0) { height =-M_PI/2.0+.01; }

		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if ( currentTime - lastTime >= 1.0 ){ // If last prinf() was more than 1sec ago
			// compute and reset
			frame_time = 1000.0/double(nbFrames);
			nbFrames = 0;
			lastTime += 1.0;
		}
		// }}}

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		// Compute and send matrices to shader {{{

		// Projection matrix : 45 Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
		glm::mat4 ProjectionMatrix = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.01f, 10.f);
		// Camera matrix
		glm::mat4 ViewMatrix       = glm::lookAt(
						glm::vec3(orbit_size*cos(orbit),orbit_size*sin(height),orbit_size*sin(orbit)), // Camera is at (4,3,3), in World Space 			// For suzanne : glm::vec3(0,0,3)
						glm::vec3(0,0,0), // and looks at the origin
						glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
						);
		// Model matrix : an identity matrix (model will be at the origin)
		glm::mat4 ModelMatrix      = glm::mat4(1.0f);
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		// Send our transformation to the currently bound shader,
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);

		glm::vec3 lightPos = glm::vec3(lx,ly,lz); // for arma, light ain't that bright ...
		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

		glUniform1f(max_valence_shader_field, static_cast<float>(object_model.get_max_valence()));

		// }}}

		// Set up arrays to be used by the shaders and draw them {{{

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture);
		// Set our "myTextureSampler" sampler to use Texture Unit 0
		glUniform1i(TextureID, 0);

		// 1rst attribute buffer : vertices {{{
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
				0,                  // attribute
				3,                  // size
				GL_FLOAT,           // type
				GL_FALSE,           // normalized?
				0,                  // stride
				(void*)0            // array buffer offset
		);
		// }}}

		// 2nd attribute buffer : UVs {{{
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
				1,                                // attribute
				2,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
		);
		// }}}

		// 3rd attribute buffer : normals {{{
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glVertexAttribPointer(
				2,                                // attribute
				3,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
		);
		// }}}

		// 4th attribute buffer : vertex valences {{{
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, valenceBuffer);
		glVertexAttribPointer(
				3,                                // attribute
				1,                                // size
				GL_FLOAT,                // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
		);
		// }}}

		glUniform1ui(glGetUniformLocation(programID, "maxValence"), object_model.get_max_valence());

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		// Draw normals if asked to :
		if (not show_normals) {
		// Draw the triangles {{{
		glDrawElements(
			GL_TRIANGLES,				// mode
			object_model.get_triangle_count() * 3,	// count
			GL_UNSIGNED_SHORT,			// type
			(void*)0				// element array buffer offset
		);
		// }}}

		// Draw a unit cube if necessary {{{
		if (unit_cube) {
			glm::vec3 p0 = glm::vec3( 1.0, -1.0, -1.0);
			glm::vec3 p1 = glm::vec3(-1.0, -1.0, -1.0);
			glm::vec3 p2 = glm::vec3(-1.0,  1.0, -1.0);
			glm::vec3 p3 = glm::vec3( 1.0,  1.0, -1.0);
			glm::vec3 p4 = glm::vec3( 1.0, -1.0,  1.0);
			glm::vec3 p5 = glm::vec3(-1.0, -1.0,  1.0);
			glm::vec3 p6 = glm::vec3(-1.0,  1.0,  1.0);
			glm::vec3 p7 = glm::vec3( 1.0,  1.0,  1.0);
			glBegin(GL_LINES);
				glLineWidth(2.0f);
				glVertex3f(p0.x, p0.y, p0.z);
				glVertex3f(p4.x, p4.y, p4.z);

				glVertex3f(p1.x, p1.y, p1.z);
				glVertex3f(p5.x, p5.y, p5.z);
				
				glVertex3f(p2.x, p2.y, p2.z);
				glVertex3f(p6.x, p6.y, p6.z);
				
				glVertex3f(p3.x, p3.y, p3.z);
				glVertex3f(p7.x, p7.y, p7.z);

				{
					const glm::vec3& pb = p0;
					const glm::vec3& pe = p1;
					glVertex3f(pb.x, pb.y, pb.z);
					glVertex3f(pe.x, pe.y, pe.z);
				}
				{
					const glm::vec3& pb = p1;
					const glm::vec3& pe = p2;
					glVertex3f(pb.x, pb.y, pb.z);
					glVertex3f(pe.x, pe.y, pe.z);
				}
				{
					const glm::vec3& pb = p2;
					const glm::vec3& pe = p3;
					glVertex3f(pb.x, pb.y, pb.z);
					glVertex3f(pe.x, pe.y, pe.z);
				}
				{
					const glm::vec3& pb = p3;
					const glm::vec3& pe = p0;
					glVertex3f(pb.x, pb.y, pb.z);
					glVertex3f(pe.x, pe.y, pe.z);
				}
				{
					const glm::vec3& pb = p4;
					const glm::vec3& pe = p5;
					glVertex3f(pb.x, pb.y, pb.z);
					glVertex3f(pe.x, pe.y, pe.z);
				}
				{
					const glm::vec3& pb = p5;
					const glm::vec3& pe = p6;
					glVertex3f(pb.x, pb.y, pb.z);
					glVertex3f(pe.x, pe.y, pe.z);
				}
				{
					const glm::vec3& pb = p6;
					const glm::vec3& pe = p7;
					glVertex3f(pb.x, pb.y, pb.z);
					glVertex3f(pe.x, pe.y, pe.z);
				}
				{
					const glm::vec3& pb = p7;
					const glm::vec3& pe = p4;
					glVertex3f(pb.x, pb.y, pb.z);
					glVertex3f(pe.x, pe.y, pe.z);
				}
			glEnd();
		}
		// }}}
		} else {
			glLineWidth(2.0f);
			glDrawElements(
				GL_LINES,
				object_model.get_vertex_count() * 2,
				GL_UNSIGNED_SHORT,
				(void*)0
			);
		}

		// }}}

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0 );

	// Cleanup VBO and shader {{{
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteBuffers(1, &normalbuffer);
	glDeleteBuffers(1, &elementbuffer);
	glDeleteProgram(programID);
	glDeleteTextures(1, &Texture);
	glDeleteVertexArrays(1, &VertexArrayID);
	// }}}

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

// vim:foldmethod=marker:foldmarker={{{,}}}:tabstop=8:
