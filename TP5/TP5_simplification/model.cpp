#include "model.hpp"

#include <iostream>
#include <algorithm>
#include <tuple>

#define EPSILON 1e-2

#include "../common/objloader.hpp"

/* constructors {{{ */
model::model() {
	this->ordered_vertices.clear();
	this->ordered_uvs.clear();
	this->ordered_normals.clear();
	this->ordered_valences.clear();
	this->indices.clear();
	this->indexed_triangles.clear();
	this->load("./bunny_simplified.off");
}

model::model(const std::string& path_name) {
	this->ordered_vertices.clear();
	this->ordered_uvs.clear();
	this->ordered_normals.clear();
	this->ordered_valences.clear();
	this->indices.clear();
	this->indexed_triangles.clear();
	this->load(path_name);
}
/* }}} */

/* Loads a model from the filesystem {{{ */
void model::load(const std::string &path_name) {
	loadOFF(path_name, this->ordered_vertices, this->indices, this->indexed_triangles);

	std::cout << "Nb vertices : " << this->ordered_vertices.size() << std::endl;
	std::cout << "Nb triangles: " << this->indices.size()/3 << std::endl;
}
/* }}} */

/* Size accessors {{{ */
const size_t model::get_vertex_count() const noexcept {
	return this->ordered_vertices.size();
}

const size_t model::get_triangle_count() const noexcept {
	return this->indexed_triangles.size();
}

const size_t model::get_normal_count() const noexcept {
	return this->ordered_normals.size();
}

const size_t model::get_uv_count() const noexcept {
	return this->ordered_uvs.size();
}
/* }}} */

/* const variable accessors {{{ */
const glm::vec3& model::get_first_vertex() const noexcept {
	return this->ordered_vertices[0];
}

const ushort& model::get_first_triangle() const noexcept {
	return this->indices[0];
}

const glm::vec3& model::get_first_normal() const noexcept {
	return this->ordered_normals[0];
}

const glm::vec2& model::get_first_uv() const noexcept {
	return this->ordered_uvs[0];
}

const uint& model::get_first_valence() const noexcept {
	return this->ordered_valences[0];
}

const ushort& model::get_first_index() const noexcept {
	return this->indices[0];
}
/* }}} */

/* computes normals per triangle {{{ */
std::vector<glm::vec3> model::compute_triangle_normals() const {
	std::vector<glm::vec3> triangle_normals;
	triangle_normals.resize(indices.size()); // should resize to the good number of triangles in the mesh

	// for all triangles :
	for (size_t i = 0; i < this->indexed_triangles.size(); ++i) {
		// get one vector :
		glm::vec3 v1 = this->ordered_vertices[this->indexed_triangles[i][1]] - this->ordered_vertices[this->indexed_triangles[i][0]];
		glm::vec3 v2 = this->ordered_vertices[this->indexed_triangles[i][2]] - this->ordered_vertices[this->indexed_triangles[i][0]];
		triangle_normals[i] = glm::cross(v1, v2);
	}
	return triangle_normals;
}
/* }}} */

/* recenters and scales the model {{{ */
void model::center_and_scale_model() {
	// iterate on it, and compute barycenter
	glm::vec3 bary = glm::vec3(.0,.0,.0);
	for (const glm::vec3& cur_vertex : this->ordered_vertices) {
		bary += cur_vertex;
	}
	bary /= static_cast<float>(this->ordered_vertices.size());
	// iterate on it, and find max value.
	float max_dist_from_center = .0f;
	for (glm::vec3& cur_vertex : this->ordered_vertices) {
		cur_vertex -= bary;
		if (glm::length(cur_vertex) > max_dist_from_center) {
			max_dist_from_center = glm::length(cur_vertex);
		}
	}
	for (glm::vec3& cur_vertex : this->ordered_vertices) {
		cur_vertex /= max_dist_from_center;
	}
	return;
}
/* }}} */

/* computes smooth vertex normals {{{ */
void model::compute_smooth_vertex_normals(size_t mode) {
	typedef struct weighted_normal {
		const glm::vec3 normal;
		float area;
	} normal_t;

	// Triangle normals and triangle areas
	std::vector<normal_t> triangle_normals;
	// Positions of normals per vertex
	std::vector<std::vector<unsigned short>> normals_per_vertex;
	normals_per_vertex.resize(this->ordered_vertices.size());

	// Weights of normals per vertex
	std::vector<std::vector<float>> normal_weights;
	normal_weights.resize(this->ordered_vertices.size());

	for (size_t i = 0; i < this->indexed_triangles.size(); ++i) {
		const std::vector<unsigned short>& triangle = this->indexed_triangles[i];
		// compute triangle area
		float tri_area = compute_triangle_area(this->ordered_vertices, triangle);
		// compute normal
		const auto& v1 = this->ordered_vertices[triangle[0]];
		const auto& v2 = this->ordered_vertices[triangle[1]];
		const auto& v3 = this->ordered_vertices[triangle[2]];
		glm::vec3 va = v2 - v1;
		glm::vec3 vb = v3 - v1;
		// get normal struct :
		normal_t current_normal{glm::cross(va,vb),tri_area};
		triangle_normals.push_back(current_normal);
		// add triangle index to adjacency list for vertexes :
		normals_per_vertex[triangle[0]].push_back(i);
		normals_per_vertex[triangle[1]].push_back(i);
		normals_per_vertex[triangle[2]].push_back(i);
	}
	if (mode == 1) {
		// Update normals, and compute weights based on area
		for (size_t i = 0; i < this->ordered_vertices.size(); ++i) {
			// get triangles adjacent to this vertex :
			const std::vector<unsigned short>& adjacent_triangles = normals_per_vertex[i];
			// sum up all areas :
			float total_area = .0f;
			for (const auto& tri : adjacent_triangles) {
				total_area += triangle_normals[tri].area;
			}
			// add all normal weights to it.
			for (const auto& tri : adjacent_triangles) {
				normal_weights[i].push_back( triangle_normals[tri].area / total_area );
			}
		}
	} else {
		// otherwise, just make a regular mean of all normals
		for (size_t i = 0; i < this->ordered_vertices.size(); ++i) {
			size_t adjacency_size = normals_per_vertex[i].size();
			normal_weights[i] = std::vector<float>(adjacency_size, 1.0f/static_cast<float>(adjacency_size));
		}
	}

	this->ordered_normals.clear();
	this->ordered_normals = std::vector<glm::vec3>(this->ordered_vertices.size(), glm::vec3(.0f,.0f,.0f));

	// update normals
	for (size_t i = 0; i < this->ordered_vertices.size(); ++i) {
		for (size_t j = 0; j < normals_per_vertex[i].size(); ++j) {
			const std::vector<ushort>& tri = normals_per_vertex[i];
			this->ordered_normals[i] += triangle_normals[tri[j]].normal * normal_weights[i][j];
		}
		this->ordered_normals[i] = glm::normalize(this->ordered_normals[i]);
	}
}
/* }}} */

/* computes the area of a triangle using Heron's formula {{{ */
double compute_triangle_area(const std::vector<glm::vec3>& vertices, const std::vector<ushort>& indices) {
	// using Heron's formula
	glm::vec3 A = vertices[indices[1]] - vertices[indices[0]];	double a = glm::length(A);
	glm::vec3 B = vertices[indices[2]] - vertices[indices[1]];	double b = glm::length(B);
	glm::vec3 C = vertices[indices[0]] - vertices[indices[2]];	double c = glm::length(C);
	double p = (a+b+c)/2.0; // perimeter

	return glm::sqrt(p * (p-a) * (p-b) * (p-c));
}
/* }}} */

/* Collecter le 1-voisinnage d'un point {{{ */
std::vector<std::vector<unsigned short>> model::collect_one_ring_for_all_vertices() const {
	std::vector<std::vector<unsigned short>> one_ring;
	one_ring.resize (this->ordered_vertices.size ());
	// for all triangles
	for (unsigned int i = 0; i < this->indexed_triangles.size (); i++) {
		const std::vector<unsigned short> & ti = this->indexed_triangles[i];
		// for all the vertices in this triangle
		for (unsigned int j = 0; j < 3; j++) {
			unsigned short vj = ti[j];
			// check all other triangle vertices
			for (unsigned int k = 1; k < 3; k++) {
				unsigned int vk = ti[(j+k)%3];
				// if t[k] not into the 1ring of t[j], add it :
				if (std::find (one_ring[vj].begin (), one_ring[vj].end (), vk) == one_ring[vj].end ())
					one_ring[vj].push_back (vk);
			}
		}
	}
	return one_ring;
}
/* }}} */

/* Calcule la valence des points du modèle {{{ */
void model::compute_vertex_valences() {
	std::vector<std::vector<ushort>> one_ring = this->collect_one_ring_for_all_vertices();
	this->ordered_valences.resize(one_ring.size());
	unsigned int max = 0;
	for (size_t i = 0; i < one_ring.size(); ++i) {
		this->ordered_valences[i] = one_ring[i].size();
		if (this->ordered_valences[i] > max) {
			max = this->ordered_valences[i];
		}
	}
	std::cerr << "Max valence in model was " << max << std::endl;
}
/* }}} */

/* Returns the maximum valence of all vertices in the mesh {{{ */
size_t model::get_max_valence() const noexcept {
	unsigned int max = 0;
	for (const auto& v : this->ordered_valences) {
		if (v > max) {
			max = v;
		}
	}
	return max;
}
/* }}} */

/* Get one ring, the wrong way {{{ */
std::vector<std::vector<unsigned short>> model::get_one_ring() {
	std::vector<std::vector<unsigned short>> one_ring;
	one_ring.resize (this->ordered_vertices.size ());
	// for all triangles
	for (unsigned int i = 0; i < this->indexed_triangles.size (); i++) {
		const std::vector<unsigned short> & ti = this->indexed_triangles[i];
		// for all the vertices in this triangle
		for (unsigned int j = 0; j < 3; j++) {
			unsigned short vj = ti[j];
			// check all other triangle vertices
			for (unsigned int k = 1; k < 3; k++) {
				unsigned int vk = ti[(j+k)%3];
				// if t[k] not into the 1ring of t[j], add it :
				if (std::find (one_ring[vj].begin (), one_ring[vj].end (), vk) == one_ring[vj].end ())
					one_ring[vj].push_back (vk);
				}
		}
	}
	return one_ring;
}
/* }}} */

/* Draw normals in a glBegin context {{{ */
void model::debug_draw_normals() const {
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	for(size_t i = 0; i < this->ordered_vertices.size(); ++i) {
		const glm::vec3& v = this->ordered_vertices[i];
		const glm::vec3& n = this->ordered_normals[i];
		glVertex3f(v.x, v.y, v.z);
		glVertex3f(v.x+n.x, v.y+n.y, v.z+n.z);
	}
	glEnd();
}
/* }}} */

/* Get vertices for array drawing {{{ */
std::vector<glm::vec3> model::get_vertices_and_normals() const {
	std::vector<glm::vec3> full_data;
	for (size_t i = 0; i < this->ordered_vertices.size(); ++i) {
		full_data.push_back( this->ordered_vertices[i] );
		full_data.push_back( this->ordered_vertices[i] + ( this->ordered_normals[i] ) );
	}
	return full_data;
}
/* }}} */

/* Get elements to draw for VBO {{{ */
std::vector<ushort> model::get_elements_vertices_normals() const {
	std::vector<ushort> element_data;
	size_t offset = this->ordered_vertices.size()*2;
	for(size_t i = 0; i < offset; ++i) {
		element_data.push_back(static_cast<unsigned short>(i));
	}
	return element_data;
}
/* }}} */

/* Gets boundary box of the model {{{ */
std::pair<glm::vec3,glm::vec3> model::get_boundary_box() const {
	float min_x, min_y, min_z;
	float max_x, max_y, max_z;
	min_x = min_y = min_z = std::numeric_limits<float>::max();
	max_x = max_y = max_z = std::numeric_limits<float>::min();
	for (const auto& v : this->ordered_vertices) {
		if (v.x < min_x) { min_x = v.x; }
		if (v.y < min_y) { min_y = v.y; }
		if (v.x < min_z) { min_z = v.z; }
		if (v.x > max_x) { max_x = v.x; }
		if (v.y > max_y) { max_y = v.y; }
		if (v.x > max_z) { max_z = v.z; }
	}
	return std::make_pair(glm::vec3(min_x-EPSILON,min_y-EPSILON,min_z-EPSILON),glm::vec3(max_x+EPSILON,max_y+EPSILON,max_z+EPSILON));
}
/* }}} */

/* Gets square boundary box of the model {{{ */
std::pair<glm::vec3,glm::vec3> model::get_square_boundary_box() const {
	glm::vec3 l, h;
	std::tie(l,h) = this->get_boundary_box();
	float dx = std::abs(l.x - h.x);
	float dy = std::abs(l.y - h.y);
	float dz = std::abs(l.z - h.z);
	float da = .0f, db = .0f;
	if (dx > dy) {
		if (dx > dz) {
			da = std::abs(dx - dy) / 2.0f;
			db = std::abs(dx - dz) / 2.0f;
			return std::make_pair(
				glm::vec3(l.x, l.y-da, l.z-db),
				glm::vec3(h.x, h.y+da, h.z+db)
			);
		} else {
			da = std::abs(dz - dx) / 2.0f;
			db = std::abs(dz - dy) / 2.0f;
			return std::make_pair(
				glm::vec3(l.x-da, l.y-db, l.z),
				glm::vec3(h.x+da, h.y+db, h.z)
			);
		}
	} else {
		if (dy > dz) {
			da = std::abs(dy - dx) / 2.0f;
			db = std::abs(dy - dz) / 2.0f;
			return std::make_pair(
				glm::vec3(l.x-da, l.y, l.z-db),
				glm::vec3(h.x+da, h.y, h.z+db)
			);
		} else {
			da = std::abs(dz - dx) / 2.0f;
			db = std::abs(dz - dy) / 2.0f;
			return std::make_pair(
				glm::vec3(l.x-da, l.y-db, l.z),
				glm::vec3(h.x+da, h.y+db, h.z)
			);
		}
	}
}
/* }}} */

/* Scale the model using an index {{{ */
void model::scale(float s) {
	for(glm::vec3& v : this->ordered_vertices) {
		v *= s;
	}
}
/* }}} */

/* Scale the model using an index {{{ */
void model::scale(float sx, float sy, float sz) {
	for(glm::vec3& v : this->ordered_vertices) {
		v.x *= sx;
		v.y *= sy;
		v.z *= sz;
	}
}
/* }}} */

/* Translate a model by a vector {{{ */
void model::translate(glm::vec3 t) {
	for (glm::vec3& v : this->ordered_vertices) {
		v += t;
	}
}
/* }}} */

/* Simplify the model with a grid of size resolution * resolution * resolution {{{ */
void model::simplify(size_t resolution) {
	// Get the model into a cube_res * cube_res * cube_res grid at the origin {{{
	glm::vec3 lb, ub; // lb = lower bound, ub = upper bound
	std::tie(lb,ub) = this->get_square_boundary_box();

	// Get the edge of the model's bounding box to be
	// at (0,0,0) for easier scaling and cube identification
	this->translate(-lb);
	ub -= lb;
	// get the model to get coordinates in [0,1]
	float s = std::sqrt(3.0f) / glm::length(ub);
	this->scale(s, s, s);
	float cube_res = static_cast<float>(resolution);
	this->scale(cube_res,cube_res,cube_res);
	// }}}

	// Apply simplification {{{
	// initialise vector to original size and contents (0 vertices per cube for the moment)

	// new vertices positions to be determined by the algorithm :
	std::vector<glm::vec3> new_vertices;
	// new normals for new vertices to be determined by the algorithm :
	std::vector<glm::vec3> new_normals;
	// stores the indices of the vertices in the grid (x,y,z)
	std::vector<std::vector<std::vector<std::vector<size_t>>>> vertex_indices = std::vector<std::vector<std::vector<std::vector<size_t>>>>(resolution, std::vector<std::vector<std::vector<size_t>>>(resolution,std::vector<std::vector<size_t>>(resolution,std::vector<size_t>())));
	// initialise a grid of grid centers :
	std::vector<std::vector<std::vector<glm::vec3>>> grid_centers;
	std::vector<std::vector<std::vector<glm::vec3>>> grid_normals;
	grid_centers.resize(resolution);
	grid_normals.resize(resolution);
	for (size_t i = 0; i < grid_centers.size(); ++i) {
		grid_centers[i].resize(resolution);
		grid_normals[i].resize(resolution);
		for (size_t j = 0; j < grid_centers[i].size(); ++j) {
			grid_centers[i][j].resize(resolution);
			grid_normals[i][j].resize(resolution);
			for (size_t k = 0; k < grid_centers[i][j].size(); ++k) {
				grid_centers[i][j][k] = glm::vec3(
					static_cast<float>(i)+.5f,
					static_cast<float>(j)+.5f,
					static_cast<float>(k)+.5f
				);
				grid_normals[i][j][k] = glm::vec3(.0f,.0f,.0f);
			}
		}
	}
	// iterate on the vertices, to place them in a grid and add them to grid centers :
	for (size_t v_it = 0; v_it < this->ordered_vertices.size(); ++v_it) {
		const glm::vec3 v = this->ordered_vertices[v_it];
		size_t x = static_cast<size_t>(std::floor(v.x));
		size_t y = static_cast<size_t>(std::floor(v.y));
		size_t z = static_cast<size_t>(std::floor(v.z));
		vertex_indices[x][y][z].push_back(v_it);
		grid_centers[x][y][z] += glm::vec3(v.x, v.y, v.z);
	}
	// compute "real" coordinates of the grid centers, normalized :
	for (size_t x_it = 0; x_it < resolution; ++x_it) {
		for (size_t y_it = 0; y_it < resolution; ++y_it) {
			for (size_t z_it = 0; z_it < resolution; ++z_it) {
				if (vertex_indices[x_it][y_it][z_it].size() > 0) {
					grid_centers[x_it][y_it][z_it] *= (1.0f / static_cast<float>(vertex_indices[x_it][y_it][z_it].size()));
					grid_normals[x_it][y_it][z_it] = glm::normalize(grid_normals[x_it][y_it][z_it]);
					// We won't need the vertex indices anymore, so we use the vector
					// to store the new vertex indices (grid centers) for the new
					// vertex vector :
					vertex_indices[x_it][y_it][z_it].clear();
					vertex_indices[x_it][y_it][z_it].push_back(new_vertices.size());
					new_vertices.push_back(grid_centers[x_it][y_it][z_it]);
					new_normals.push_back(grid_normals[x_it][y_it][z_it]);
				}
			}
		}
	}
	std::vector<unsigned short> new_indices;
	std::vector<std::vector<unsigned short>> new_indexed_triangles;
	// iterate on the triangles :
	for (size_t t = 0; t < this->indexed_triangles.size(); ++t) {
		const std::vector<unsigned short>& triangle = this->indexed_triangles[t];
		glm::uvec3 v1_grid_pos = get_grid_coords(this->ordered_vertices[triangle[0]]);
		glm::uvec3 v2_grid_pos = get_grid_coords(this->ordered_vertices[triangle[1]]);
		glm::uvec3 v3_grid_pos = get_grid_coords(this->ordered_vertices[triangle[2]]);
		if (v1_grid_pos != v2_grid_pos && v2_grid_pos != v3_grid_pos && v3_grid_pos != v1_grid_pos) {
			// all vertices are in a different center :
			unsigned short a = static_cast<unsigned short>(vertex_indices[v1_grid_pos.x][v1_grid_pos.y][v1_grid_pos.z][0]);
			unsigned short b = static_cast<unsigned short>(vertex_indices[v2_grid_pos.x][v2_grid_pos.y][v2_grid_pos.z][0]);
			unsigned short c = static_cast<unsigned short>(vertex_indices[v3_grid_pos.x][v3_grid_pos.y][v3_grid_pos.z][0]);
			new_indices.push_back(a);
			new_indices.push_back(b);
			new_indices.push_back(c);
			new_indexed_triangles.push_back( (std::vector<unsigned short>{a,b,c}) );
		}
	}
	std::cerr << "Computed and tesselated new model. Putting back into model." << std::endl;
	this->ordered_vertices = new_vertices;
	this->ordered_normals = new_normals;
	this->indices = new_indices;
	this->indexed_triangles = new_indexed_triangles;
	// cleanup :
	vertex_indices.clear();
	new_vertices.clear();
	new_normals.clear();
	new_indices.clear();
	new_indexed_triangles.clear();
	grid_centers.clear();
	grid_normals.clear();
	// }}}
	
	// Un-scale the model back to it's original size and location {{{
	this->scale(1.0f/cube_res,1.0f/cube_res,1.0f/cube_res);
	this->scale(1.0f/s,1.0f/s,1.0f/s);

	this->translate(lb);
	// }}}
}
/* }}} */

glm::uvec3 get_grid_coords(const glm::vec3& v) {
	size_t x = static_cast<size_t>(std::floor(v.x));
	size_t y = static_cast<size_t>(std::floor(v.y));
	size_t z = static_cast<size_t>(std::floor(v.z));
	return glm::uvec3(x,y,z);
}

// vim:foldmethod=marker:foldmarker={{{,}}}:tabstop=8:
