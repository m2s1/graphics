#ifndef _SIMPLIFATION_MODEL_HPP_
#define _SIMPLIFATION_MODEL_HPP_

#include <vector>
#include <string>

#include <glm/glm.hpp>

#include <GL/glew.h>

class model {
	private:
		std::vector<glm::vec3> ordered_vertices;
		std::vector<glm::vec3> ordered_normals;
		std::vector<glm::vec2> ordered_uvs;
		std::vector<unsigned short> indices;
		std::vector<std::vector<unsigned short>> indexed_triangles;
		std::vector<unsigned int> ordered_valences;
	public:
		bool enable_normals = false;
		/* default constructor, loads bunny.off */
		model();
		/* constructor loading file at path_name */
		model(const std::string& path_name);
		/* loads model at ./path_name */
		void load(const std::string& path_name);
		/* gets the number of vertices */
		const size_t get_vertex_count() const noexcept;
		/* gets the number of triangles */
		const size_t get_triangle_count() const noexcept;
		/* gets the number of normals */
		const size_t get_normal_count() const noexcept;
		/* gets the number of uvs on the model (always == vertices, even when none loaded) */
		const size_t get_uv_count() const noexcept;
		/* gets adress of first triangle for VBO indexing */
		const glm::vec3& get_first_vertex() const noexcept;
		/* gets adress of first triangle for VBO indexing */
		const ushort& get_first_triangle() const noexcept;
		/* gets adress of first normal for VBO indexing */
		const glm::vec3& get_first_normal() const noexcept;
		/* gets adress of first uv for VBO indexing */
		const glm::vec2& get_first_uv() const noexcept;
		/* gets first index */
		const ushort& get_first_index() const noexcept;
		/* gets adress of first vertex valence for VBO indexing */
		const uint& get_first_valence() const noexcept;
		/* compute each triangle's normals */
		std::vector<glm::vec3> compute_triangle_normals() const;
		/* centers and scales model to the [-1,1] range */
		void center_and_scale_model();
		/* collects the one_ring of all vertices */
		std::vector<std::vector<unsigned short>> collect_one_ring_for_all_vertices() const;
		/* computes the normal per vertex, smoothly */
		void compute_smooth_vertex_normals(size_t mode);
		/* compute vertex valences */
		void compute_vertex_valences();
		/* returns max valence */
		size_t get_max_valence() const noexcept;
		/* for smooth normals */
		std::vector<std::vector<unsigned short>> get_one_ring();
		/* draw normals */
		void debug_draw_normals() const;
		/* get vectors for showing points and normals */
		std::vector<glm::vec3> get_vertices_and_normals() const;
		/* get element vector for showing points and normals */
		std::vector<ushort> get_elements_vertices_normals() const;
		/* Get boundary box */
		std::pair<glm::vec3,glm::vec3> get_boundary_box() const;
		/* Get boundary box */
		std::pair<glm::vec3,glm::vec3> get_square_boundary_box() const;
		/* apply a scale on the whole model */
		void scale(float);
		/* apply a scale on the whole model */
		void scale(float, float, float);
		/* move the whole model by a vector */
		void translate(glm::vec3);
		/* simplify the model using a grid of size resolution * resolution * resolution */
		void simplify(size_t resolution);
		const std::vector<glm::vec3>& get_vertices() const { return this->ordered_vertices; }
};

double compute_triangle_area(const std::vector<glm::vec3>&, const std::vector<unsigned short>&);

glm::uvec3 get_grid_coords(const glm::vec3&);

#endif // _SIMPLIFATION_MODEL_HPP_

// vim:foldmethod=marker:foldmarker={{{,}}}:tabstop=8:
