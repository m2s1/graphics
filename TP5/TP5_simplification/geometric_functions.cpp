#include "geometric_functions.hpp"

void center_and_scale_model(std::vector<glm::vec3>& vertices) {
	// iterate on it, and compute barycenter
	glm::vec3 bary = glm::vec3(.0,.0,.0);
	for (const glm::vec3& cur_vertex : vertices) {
		bary += cur_vertex;
	}
	bary /= static_cast<float>(vertices.size());
	std::cout << "Centered model ... ";
	// iterate on it, and find max value.
	glm::length_t max_dist_from_center = .0f;
	for (glm::vec3& cur_vertex : vertices) {
		cur_vertex -= bary;
		if (glm::length(cur_vertex) > max_dist_from_center) {
			max_dist_from_center = glm::length(cur_vertex);
		}
	}
	std::cout << "found max value ... " ;
	for (glm::vec3& cur_vertex : vertices) {
		cur_vertex /= max_dist_from_center;
	}
	std::cout << "and normalised model." << std::endl;
	//
	return;
}

void compute_vertex_valences (const std::vector<std::vector<unsigned short>>& one_ring, std::vector<unsigned int> & valences, unsigned int &maxVal) {
	valences.resize(one_ring.size());
	unsigned int max = 0;
	for (size_t i = 0; i < one_ring.size(); ++i) {
		valences[i] = one_ring[i].size();
		if (valences[i] > max) {
			max = valences[i];
		}
	}
	std::cout << "Max valence in the mesh : " << max << std::endl;
	maxVal = max;
}

void collect_one_ring_and_compute_valence(	const std::vector<glm::vec3>& vertices,
						const std::vector<std::vector<unsigned short>>& vertex_indices,
						std::vector<unsigned int>& valences,
						unsigned int & maxVal,
						std::vector<std::vector<unsigned short> > & one_ring) {
	collect_one_ring(vertices, vertex_indices, one_ring);
	compute_vertex_valences(one_ring,valences, maxVal);
}


void compute_smooth_vertex_normals (
		unsigned int weight_type,
		const std::vector<glm::vec3> & vertices,
		const std::vector<unsigned short> & indices,
		const std::vector<std::vector<unsigned short>> & vertex_indices,
		std::vector<unsigned int>& vertex_valences,
		unsigned int & maxVal,
		std::vector<glm::vec3> & vertex_normals) {
	float w = 1.0; // default: uniform weights

	// compute normals per vertices

	std::vector<std::vector<unsigned short>> one_ring ;
	// ask for the 1-ring of each vertices, to get all neighboring vertices
	collect_one_ring_and_compute_valence(vertices, vertex_indices, vertex_valences, maxVal, one_ring);
	std::vector<glm::vec3> triangle_normals;
	compute_triangle_normals(vertices, vertex_indices, triangle_normals);

	// clear the vector of normals :
	vertex_normals.clear();
	vertex_normals.resize(vertices.size());

	std::vector<std::vector<double>> normalWeights;
	normalWeights.resize(vertices.size());

	if (weight_type == 1) { // area weight
		// weight normals according to the area of the face
		for (unsigned int i = 0; i < one_ring.size(); ++i) {
			std::vector<double> areas = std::vector<double>();
			double totalarea = 0.0;
			// for each vertex get the area of the triangles :
			for(size_t j = 0; j < one_ring[i].size(); ++j ){
				// get the area of each triangle :
				areas.push_back(compute_triangle_area(vertices, vertex_indices[one_ring[i][j]]));
				// add it to the total area :
				totalarea += areas[j];
			}
			for (unsigned int j = 0; j < one_ring[i].size(); ++j) {
				areas[j] /= totalarea;
			}
			normalWeights[i] = std::vector<double>(areas);
		}
	} else if (weight_type == 2) { // angle weight

	} else {
		for (unsigned int i = 0; i < one_ring.size(); ++i) {
			normalWeights[i].resize(one_ring[i].size());
			std::fill(normalWeights[i].begin(), normalWeights[i].end(), 1.0/((double)one_ring[i].size()));
		}
	}

	for (unsigned int i = 0; i < vertices.size(); ++i) {
		glm::vec3 finalNormal = glm::vec3(0.0);
		for (unsigned int j = 0; j < one_ring[i].size(); ++j) {
			glm::vec3 curNorm = glm::vec3(triangle_normals[one_ring[i][j]]);
			curNorm *= normalWeights[i][j];
			finalNormal+=curNorm;
		}
		vertex_normals[i] = finalNormal;
	}
}

double compute_triangle_area(const std::vector<glm::vec3>& vertices, const std::vector<unsigned short>& indices) {
	// using Heron's formula
	glm::vec3 A = vertices[indices[1]] - vertices[indices[0]];	double a = glm::length(A);
	glm::vec3 B = vertices[indices[2]] - vertices[indices[1]];	double b = glm::length(B);
	glm::vec3 C = vertices[indices[0]] - vertices[indices[2]];	double c = glm::length(C);
	double p = (a+b+c)/2.0; // perimeter

	return glm::sqrt(p * (p-a) * (p-b) * (p-c));
}

void compute_triangle_normals (const std::vector<glm::vec3>& vertices,
				const std::vector<std::vector<unsigned short>>& indices,
				std::vector<glm::vec3> & triangle_normals){


	triangle_normals.resize(indices.size()); // should resize to the good number of triangles in the mesh

	// for all triangles :
	for (size_t i = 0; i < indices.size(); ++i) {
		// get one vector :
		glm::vec3 v1 = vertices[indices[i][1]] - vertices[indices[i][0]];
		glm::vec3 v2 = vertices[indices[i][2]] - vertices[indices[i][0]];
		/*if (glm::dot(v1, v2) < 0) {
			v2 = -v2;
		}*/
		triangle_normals[i] = glm::cross(v1, v2);
	}

}

void calc_uniform_mean_curvature(const std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& vunicurvature_,
		const std::vector<unsigned short>& indices, const std::vector<std::vector<unsigned short>> & vertex_indices) {
	// get all one-ring for vertices :
	std::vector<std::vector<unsigned short>> one_ring;
	collect_one_ring(vertices, vertex_indices, one_ring);

	// iterate on them :
	for (size_t i = 0; i < one_ring.size(); ++i) {
		glm::vec3 current_vertex_laplace = glm::vec3(.0,.0,.0);
		if (one_ring[i].size() > 0) {
			for (size_t j = 0; j < one_ring[i].size(); ++j) {
				current_vertex_laplace += glm::vec3(vertices[one_ring[i][j]]);
			}
			vunicurvature_.push_back((current_vertex_laplace / static_cast<float>(one_ring[i].size())) - vertices[i]);
		} else {
			std::cout << "valence of 0 on vertex " << i << ". check your shit." << std::endl;
			vunicurvature_.push_back(glm::vec3(.0f,.0f,.0f));
		}
	}
}

void calc_uniform_smoothing(size_t nb_iter, std::vector<glm::vec3>& vertices, const std::vector<unsigned short> &indices, const std::vector<std::vector<unsigned short> > &vertex_indices) {
	for (size_t iters = 0; iters < nb_iter; ++iters) {
		std::vector<glm::vec3> current_mean_curvature;
		calc_uniform_mean_curvature(vertices, current_mean_curvature, indices, vertex_indices);
		for (size_t i = 0; i < vertices.size(); ++i) {
			vertices[i] = vertices[i] + current_mean_curvature[i] * .5f;
		}
		current_mean_curvature.clear();
	}
}

