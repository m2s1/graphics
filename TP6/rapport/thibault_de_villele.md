# TP6 - Projections, et surfaces de points implicites

### HPSS : Surfaces de nuages de points d'Hermite

Nous démarrons donc avec un modèle, et un ensemble de points à projeter comme suit : (la seule capture que j'avais était avec la sphère, mais les projections suivantes sont faites avec les points dans un cube [-2;2]) :

![original-points](./01_regular_pointset_sphere_original.png)

Lorsque l'on projette un ensemble de points en utilisant la méthode HPSS, avec une distance variable et en utilisant un kernel gaussien, on obtient ceci :

![gaussian-projection-model](./04_gaussian_projection_result_with_model.png)

Si on ne laisse apparaitre que les points projetés, on obtient :

![gaussian-projection](./05_gaussian_projection_result_without_model.png)

Lorsque l'on utilise un noyau gaussien a taille variable, je peux remarquer que les points ont tendance à se projeter toujours sur des points aux extrême des modèles, et non forcément à la zone qui se trouve "en dessous" d'eux par rapport au modèle. Cela est surement dû au fait que j'ai mal calculé ma projection, mais bon. (les captures d'écran précédentes ont déjà une taille variable, qui est le max des distances des `k` plus proches voisins)

Lorsque l'on ajoute du bruit au modèle, on peut voir que celui ci est bien déformé :

![noise](./06_original_model_noise.png)

Et pourtant, les points se projettent bien dessus :

![noise-proj](./07_original_model_noise_projected_with_model.png)

![noise-proj-clear](./08_original_model_noise_projected_without_model.png)

On peut donc déterminer que peu importe le modèle sous-jacent et son état, la méthode HPSS parviendra toujours à projeter des points sur une surface implicite.
