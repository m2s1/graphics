#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"
#include "src/jmkdtree.h"

bool showmodel = true;

std::vector< Vec3 > positions;
std::vector< Vec3 > normals;

std::vector< Vec3 > positions2;
std::vector< Vec3 > normals2;

typedef enum available_kernel_types {
	SINGULAR,
	GAUSSIAN,
	WENDLAND
} kernel_t;

#define S_weight 1.0f

// {{{ OpenGL/GLUT application code.
static GLint window;
static unsigned int SCREENWIDTH = 640;
static unsigned int SCREENHEIGHT = 480;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;
// }}}

// I/O and some stuff (rigid transformation, subsample) {{{
void loadPN (const std::string & filename , std::vector< Vec3 > & o_positions , std::vector< Vec3 > & o_normals ) {
    unsigned int surfelSize = 6;
    FILE * in = fopen (filename.c_str (), "rb");
    if (in == NULL) {
        std::cout << filename << " is not a valid PN file." << std::endl;
        return;
    }
    size_t READ_BUFFER_SIZE = 1000; // for example...
    float * pn = new float[surfelSize*READ_BUFFER_SIZE];
    o_positions.clear ();
    o_normals.clear ();
    while (!feof (in)) {
        unsigned numOfPoints = fread (pn, 4, surfelSize*READ_BUFFER_SIZE, in);
        for (unsigned int i = 0; i < numOfPoints; i += surfelSize) {
            o_positions.push_back (Vec3 (pn[i], pn[i+1], pn[i+2]));
            o_normals.push_back (Vec3 (pn[i+3], pn[i+4], pn[i+5]));
        }

        if (numOfPoints < surfelSize*READ_BUFFER_SIZE) break;
    }
    fclose (in);
    delete [] pn;
}
void savePN (const std::string & filename , std::vector< Vec3 > const & o_positions , std::vector< Vec3 > const & o_normals ) {
    if ( o_positions.size() != o_normals.size() ) {
        std::cout << "The pointset you are trying to save does not contain the same number of points and normals." << std::endl;
        return;
    }
    FILE * outfile = fopen (filename.c_str (), "wb");
    if (outfile == NULL) {
        std::cout << filename << " is not a valid PN file." << std::endl;
        return;
    }
    for(unsigned int pIt = 0 ; pIt < o_positions.size() ; ++pIt) {
        fwrite (&(o_positions[pIt]) , sizeof(float), 3, outfile);
        fwrite (&(o_normals[pIt]) , sizeof(float), 3, outfile);
    }
    fclose (outfile);
}
void scaleAndCenter( std::vector< Vec3 > & io_positions ) {
    Vec3 bboxMin( FLT_MAX , FLT_MAX , FLT_MAX );
    Vec3 bboxMax( FLT_MIN , FLT_MIN , FLT_MIN );
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        for( unsigned int coord = 0 ; coord < 3 ; ++coord ) {
            bboxMin[coord] = std::min<float>( bboxMin[coord] , io_positions[pIt][coord] );
            bboxMax[coord] = std::max<float>( bboxMax[coord] , io_positions[pIt][coord] );
        }
    }
    Vec3 bboxCenter = (bboxMin + bboxMax) / 2.f;
    float bboxLongestAxis = std::max<float>( bboxMax[0]-bboxMin[0] , std::max<float>( bboxMax[1]-bboxMin[1] , bboxMax[2]-bboxMin[2] ) );
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        io_positions[pIt] = (io_positions[pIt] - bboxCenter) / bboxLongestAxis;
    }
}

void applyRandomRigidTransformation( std::vector< Vec3 > & io_positions , std::vector< Vec3 > & io_normals ) {
    srand(time(NULL));
    Mat3 R = Mat3::RandRotation();
    Vec3 t = Vec3::Rand(1.f);
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        io_positions[pIt] = R * io_positions[pIt] + t;
        io_normals[pIt] = R * io_normals[pIt];
    }
}

void subsample( std::vector< Vec3 > & i_positions , std::vector< Vec3 > & i_normals , float minimumAmount = 0.1f , float maximumAmount = 0.2f ) {
    std::vector< Vec3 > newPos , newNormals;
    std::vector< unsigned int > indices(i_positions.size());
    for( unsigned int i = 0 ; i < indices.size() ; ++i ) indices[i] = i;
    srand(time(NULL));
    std::random_shuffle(indices.begin() , indices.end());
    unsigned int newSize = indices.size() * (minimumAmount + (maximumAmount-minimumAmount)*(float)(rand()) / (float)(RAND_MAX));
    newPos.resize( newSize );
    newNormals.resize( newSize );
    for( unsigned int i = 0 ; i < newPos.size() ; ++i ) {
        newPos[i] = i_positions[ indices[i] ];
        newNormals[i] = i_normals[ indices[i] ];
    }
    i_positions = newPos;
    i_normals = newNormals;
}

bool save( const std::string & filename , std::vector< Vec3 > & vertices , std::vector< unsigned int > & triangles ) {
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl;

    unsigned int n_vertices = vertices.size() , n_triangles = triangles.size()/3;
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for( unsigned int v = 0 ; v < n_vertices ; ++v ) {
        myfile << vertices[v][0] << " " << vertices[v][1] << " " << vertices[v][2] << std::endl;
    }
    for( unsigned int f = 0 ; f < n_triangles ; ++f ) {
        myfile << 3 << " " << triangles[3*f] << " " << triangles[3*f+1] << " " << triangles[3*f+2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}
// }}}

// Rendering {{{

// Light initialization {{{
void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}
// }}}

// GL Environment initialization {{{
void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    glEnable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
}
// }}}

// Draw mesh {{{
void drawTriangleMesh( std::vector< Vec3 > const & i_positions , std::vector< unsigned int > const & i_triangles ) {
	if (showmodel) {
    glBegin(GL_TRIANGLES);
    for(unsigned int tIt = 0 ; tIt < i_triangles.size() / 3 ; ++tIt) {
        Vec3 p0 = i_positions[3*tIt];
        Vec3 p1 = i_positions[3*tIt+1];
        Vec3 p2 = i_positions[3*tIt+2];
        Vec3 n = Vec3::cross(p1-p0 , p2-p0);
        n.normalize();
        glNormal3f( n[0] , n[1] , n[2] );
        glVertex3f( p0[0] , p0[1] , p0[2] );
        glVertex3f( p1[0] , p1[1] , p1[2] );
        glVertex3f( p2[0] , p2[1] , p2[2] );
    }
    glEnd();
	}
}
// }}}

// Draw point cloud {{{
void drawPointSet( std::vector< Vec3 > const & i_positions , std::vector< Vec3 > const & i_normals ) {
    glBegin(GL_POINTS);
    for(unsigned int pIt = 0 ; pIt < i_positions.size() ; ++pIt) {
        glNormal3f( i_normals[pIt][0] , i_normals[pIt][1] , i_normals[pIt][2] );
        glVertex3f( i_positions[pIt][0] , i_positions[pIt][1] , i_positions[pIt][2] );
    }
    glEnd();
}
// }}}

// Draw environment {{{
void draw () {
    glPointSize(2); // for example...
if(showmodel) {
    glColor3f(0.8,0.8,1);
    drawPointSet(positions , normals);
}
    glColor3f(1,0.5,0.5);
    drawPointSet(positions2 , normals2);
}
// }}}

// OpenGL calls for drawing {{{
void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}
// }}}

// }}}

void idle () {
    glutPostRedisplay ();
}

// User keyboard input {{{
void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;

    case 'w':
        GLint polygonMode[2];
        glGetIntegerv(GL_POLYGON_MODE, polygonMode);
        if(polygonMode[0] != GL_FILL)
            glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        break;

	case 'q':
		exit(EXIT_SUCCESS);
	case 's':
		showmodel = !showmodel;
		std::cout << " show model ? " << std::boolalpha << showmodel << std::endl;
		break;
    default:
        break;
    }
    idle ();
}
// }}}

// User mouse input {{{
void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}
// }}}

// Camera movement from mouse input {{{
void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}
// }}}

// Window resize {{{
void reshape(int w, int h) {
    camera.resize (w, h);
}
// }}}

void HPSS(Vec3 inputPoint, Vec3 & outputPoint, Vec3 & outputNormal, std::vector<Vec3>const& positions, std::vector<Vec3>const& normals,BasicANNkdTree const& kdtree, kernel_t, float radius , unsigned int nbIterations= 10, unsigned int knn = 20 );

void APSS(Vec3 inputPoint, Vec3 & outputPoint, Vec3 & outputNormal, std::vector<Vec3>const& positions, std::vector<Vec3>const& normals,BasicANNkdTree const& kdtree, kernel_t, float radius , unsigned int nbIterations= 10, unsigned int knn = 20 );

int main (int argc, char ** argv) {
	// init opengl {{{
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("OpenGL : Point Processing");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);
	// }}}

    {
        // Load a first pointset, and build a kd-tree:
        loadPN("pointsets/dino.pn" , positions , normals);

        BasicANNkdTree kdtree;
        kdtree.build(positions);

        // Create a second pointset that is artificial, and project it on pointset1 using MLS techniques:
        positions2.resize( 100000 );
        normals2.resize(positions2.size());
        for( unsigned int pIt = 0 ; pIt < positions2.size() ; ++pIt ) {
            positions2[pIt] = Vec3(
                        -2.0 + 4.0 * (double)(rand())/(double)(RAND_MAX),
                        -2.0 + 4.0 * (double)(rand())/(double)(RAND_MAX),
                        -2.0 + 4.0 * (double)(rand())/(double)(RAND_MAX)
                        );
            //positions2[pIt].normalize();
            //positions2[pIt] = 0.6 * positions2[pIt];
        }

	double alpha = 0.01;
	/* Inject noise on the base model before projection : */
/*	for (size_t i = 0; i < positions.size(); ++i) {
		Vec3& p = positions[i];
		double rand_noise_addition = -(2.0*alpha) + 2.0 * (alpha * (double)(rand())/(double)(RAND_MAX));
		p += rand_noise_addition * normals[i];
	}
*/
        // PROJECT USING MLS (HPSS and APSS):
        for (size_t i = 0; i < positions2.size(); ++i) {
		const Vec3 v = Vec3(positions2[i]);
		HPSS(v, positions2[i], normals2[i], positions, normals, kdtree, kernel_t::GAUSSIAN, 0.00001, 10, 20);
	}

    }
    glutMainLoop ();
    return EXIT_SUCCESS;
}

void HPSS(
	Vec3 inputPoint, Vec3 & outputPoint, Vec3 & outputNormal,
	std::vector<Vec3>const& positions, std::vector<Vec3>const& normals,
	BasicANNkdTree const& kdtree, kernel_t k_type, float radius,
	unsigned int nbIterations, unsigned int knn
) {
	// positions are the positions of the reference model
	// they are also in the kdtree

	outputPoint = Vec3(.0,.0,.0);
	outputNormal = Vec3(.0,.0,.0);

	for (size_t i = 0; i < nbIterations; ++i) {
		ANNidxArray idxs = new ANNidx[knn];
		ANNdistArray dists = new ANNdist[knn];
		outputPoint = Vec3(.0,.0,.0);
		outputNormal = Vec3(.0,.0,.0);
		// get nearest points
		kdtree.knearest(inputPoint, knn, idxs, dists);
		// compute hermite approximate projections, used to project accurately the point :
		std::vector<Vec3> approx_points;
		// compute weights :
		std::vector<double> weights;

		ANNdist max=.0;
		for (size_t i = 0; i < knn; ++i) {
			dists[i] = std::sqrt(dists[i]);
			if (dists[i] > max) {
				max = dists[i];
			}
		}

		double h = max;

		for (size_t i = 0; i < knn; ++i) {
			approx_points.push_back(
				inputPoint - Vec3::dot(inputPoint - positions[idxs[i]], normals[idxs[i]]) * normals[idxs[i]]
			);
			double r = dists[i];
			switch (k_type) {
				case kernel_t::SINGULAR : {
					weights.push_back(std::pow((h / dists[i]), S_weight));
					break;
				}
				case kernel_t::GAUSSIAN : {
					weights.push_back( std::pow( (1.0-(r/h)), 4 ) * ( 1.0 + 4.0 * r/h ) );
					break;
				}
				case kernel_t::WENDLAND : {
					weights.push_back( std::exp( (-std::pow(r,2.0)) / (std::pow(h, 2.0)) ) );
					break;
				}
			}
		}
		double sum = .0;
		for (size_t i = 0; i < knn; ++i) {
			outputPoint += (weights[i] * approx_points[i]);
			outputNormal += (weights[i] * Vec3(normals[idxs[i]]));
			sum += weights[i];
		}

		outputPoint /= sum;
		outputNormal /= sum;
		outputNormal.normalize();
		inputPoint = outputPoint;

		delete [] idxs;
		delete [] dists;
	}
}

void APSS(
	Vec3 inputPoint, Vec3 & outputPoint, Vec3 & outputNormal,
	std::vector<Vec3>const& positions, std::vector<Vec3>const& normals,
	BasicANNkdTree const& kdtree, kernel_t k_type, float radius,
	unsigned int nbIterations, unsigned int knn
) {
	// do all iterations :
	for (size_t i = 0; i < nbIterations; ++i) {
		ANNidxArray idxs = new ANNidx[knn];
		ANNdistArray dists = new ANNdist[knn];
		outputPoint = Vec3(.0,.0,.0);
		outputNormal = Vec3(.0,.0,.0);
		// get nearest points
		kdtree.knearest(inputPoint, knn, idxs, dists);
		// compute weights :
		std::vector<double> weights;
		std::vector<double> norm_weights;

		ANNdist max=.0;
		for (size_t i = 0; i < knn; ++i) {
			dists[i] = std::sqrt(dists[i]);
			if (dists[i] > max) {
				max = dists[i];
			}
		}

		double h = max;
		double sum = .0;
		for (size_t i = 0; i < knn; ++i) {
			double r = dists[i];
			switch (k_type) {
				case kernel_t::SINGULAR : {
					weights.push_back(std::pow((h / dists[i]), S_weight));
					break;
				}
				case kernel_t::GAUSSIAN : {
					weights.push_back( std::pow( (1.0-(r/h)), 4 ) * ( 1.0 + 4.0 * r/h ) );
					break;
				}
				case kernel_t::WENDLAND : {
					weights.push_back( std::exp( (-std::pow(r,2.0)) / (std::pow(h, 2.0)) ) );
					break;
				}
			}
			sum+= weights.back();
		}
		// normalize weights
		for (size_t i = 0; i < knn; ++i) {
			norm_weights.push_back(weights[i] / sum);
		}

		// compute sphere equation, and get projection point
		// compute u4 {{{
		double u4 = .0;
		double wipini = .0;
		double wipipi = .0;
		double wjpipi = .0;
		Vec3 wipi = Vec3(.0,.0,.0);
		Vec3 wjpi = Vec3(.0,.0,.0);
		Vec3 wjni = Vec3(.0,.0,.0);
		Vec3 wini = Vec3(.0,.0,.0);
		for (size_t i = 0; i < knn; ++i) {
			wipini += weights[i] * Vec3::dot(positions[idxs[i]], normals[idxs[i]]);
			wipipi += weights[i] * Vec3::dot(positions[idxs[i]], positions[idxs[i]]);
			wjpipi += norm_weights[i] * Vec3::dot(positions[idxs[i]], positions[idxs[i]]);
			wipi += weights[i] * positions[idxs[i]];
			wini += weights[i] * normals[idxs[i]];
			wjpi += norm_weights[i] * positions[idxs[i]];
			wjni += norm_weights[i] * positions[idxs[i]];
		}
		u4 = .5 * ( (wipini - Vec3::dot(wjpi, wini)) / (wipipi - Vec3::dot(wipi, wini)) );
		// }}}

		// compute plane normal {{{
		Vec3 pnormal = wjni - 2.0 * u4 * wjpi;
		pnormal.normalize();
		// }}}

		double u0 = Vec3::dot(pnormal, wjpi) - u4 * wjpipi;
	}
}

// vim:foldmethod=marker:foldmarker={{{,}}}:tabstop=8:
