#include "gmap.hpp"
#include <algorithm>
#include <iomanip>
/*------------------------------------------------------------------------*/

// Return the application of the alpha_deg on dart
GMap::id_t GMap::alpha(degree_t degree, id_t dart) const {
	assert(dart < this->alphas.size());	// check if the dart exists
	assert(degree <= 2);		// check if the degree ain't too high
	return this->alphas.at(dart)[degree];
}

// Return the application of a composition of alphas on dart
GMap::id_t GMap::alpha(degreelist_t degree, id_t dart) const {
	assert(dart < this->alphas.size());	// check if the dart exists
	id_t cur = dart;
	for (auto deg = degree.begin(); deg != degree.end(); deg++) {
		assert(cur < this->alphas.size());
		assert(*deg <= 2);
		cur = this->alphas.at(cur)[*deg];
	}
	return cur;
}


//  Test if dart is free for alpha_degree (if it is a fixed point)
bool GMap::is_free(degree_t degrees, id_t dart) const {
	assert(dart < this->alphas.size());	// check if the dart exists
	assert(degrees <= 2);		// check if the degree ain't too high
	return this->alphas.at(dart)[degrees] == this->alphas.at(dart)[degrees];
}

/*
	Test the validity of the structure.
	Check that alpha_0 and alpha_1 are involutions with no fixed points.
*/
bool GMap::is_valid() const
{
	for (idalphamap_t::const_iterator it = this->alphas.begin(); it != this->alphas.end(); ++it) {
		const id_t& dart = it->first;
		for (degree_t deg = 0; deg < 2; ++deg) {
			const id_t& alphaDeg = it->second[deg];
			if (dart == alphaDeg) return false;
			if (dart != alpha(deg, alphaDeg)) return false;
		}
	}

	degreelist_t alphaslist = {0,2,0,2};
	for (idalphamap_t::const_iterator it = this->alphas.begin(); it != this->alphas.end(); ++it) {
		const id_t& dart = it->first;
		if (alpha(alphaslist, dart) != dart)
			return false;
	}
	return true;
}

/*
	Create a new dart and return its id.
	Set its alpha_i to itself (fixed points)
*/
GMap::id_t GMap::add_dart()
{
	id_t newId = this->maxid;
	alpha_container_t ac = alpha_container_t(newId, newId, newId);
	this->alphas[newId] = ac;
	this->maxid++;
	return newId;
}

// Link the two darts with a relation alpha_degree if they are both free.
bool GMap::link_darts(degree_t degree, id_t dart1, id_t dart2)
{
	if (is_free(degree, dart1)&&is_free(degree, dart2)) {
		this->alphas.at(dart1)[degree] = dart2;
		this->alphas.at(dart2)[degree] = dart1;
		return true;
	}
	return false;
}

/*
	Return the orbit of dart using a list of alpha relation.
	Example of use : gmap.orbit(0,[0,1]).
*/
GMap::idlist_t GMap::orbit(degreelist_t alphas, id_t dart) const
{
	idlist_t result;
	idset_t marked;
	idlist_t toprocess = {dart};
	while (toprocess.size() != 0) {
		id_t current = toprocess[0];
		toprocess.erase(toprocess.begin());
		if (marked.find(current) == marked.end()) {
			result.push_back(current);
			marked.insert(current);
			for (degree_t deg : alphas) {
				toprocess.push_back(this->alphas.at(current)[deg]);
			}
		}
	}
	return result;
}

/*
	Return the ordered orbit of dart using a list of alpha relations by applying
	repeatingly the alpha relations of the list to dart.
	Example of use. gmap.orderedorbit(0,[0,1]).
	Warning: No fixed point for the given alpha should be contained.
*/
GMap::idlist_t GMap::orderedorbit(degreelist_t list_of_alpha_value, id_t dart) const
{
	idlist_t result;
	id_t current = dart;
	unsigned char curAlphaIndex = 0;
	size_t nAlpha = list_of_alpha_value.size();
	do {
		result.push_back(current);
		current = this->alphas.at(current)[list_of_alpha_value.at(curAlphaIndex)];
		curAlphaIndex++;
		if (curAlphaIndex == nAlpha) {
			curAlphaIndex = 0;
		}
	} while (current != dart);
	return result;
}


/*
	Sew two elements of degree 'degree' that start at dart1 and dart2.
	Determine first the orbits of dart to sew and heck if they are compatible.
	Sew pairs of corresponding darts
	# and if they have different embedding  positions, merge them.
*/
bool GMap::sew_dart(degree_t degree, id_t dart1, id_t dart2)
{
	// if they are of degree 1, sew them now :
	if (degree == 1) {
		this->alphas[dart1].values[1] = dart2;
		this->alphas[dart2].values[1] = dart1;
		return true;
	}

	// if they are of degree 0 or 2, checks must be in place :
	degreelist_t alphas = (degree == 2) ? degreelist_t({0}) : degreelist_t({2});
	idlist_t orbit1 = this->orderedorbit(alphas, dart1);
	idlist_t orbit2 = this->orderedorbit(alphas, dart2);
	// if their orbits are compatible :
	if (orbit1.size() == orbit2.size()) {
		// iterate on this orbit, and link them
		for (unsigned int d = 0; d < orbit1.size(); ++d) {
			if (degree == 0) {
				this->alphas[orbit1[d]].values[degree] = orbit2[d];
				this->alphas[orbit2[d]].values[degree] = orbit1[d];
			} else {
				// if they were not linked before :
				if (this->alphas[orbit1[d]].values[2] == 0) {
					this->alphas[orbit1[d]].values[2] = orbit2[d];
					this->alphas[orbit2[d]].values[2] = orbit1[d];
				} else {
					// else they were linked to smth before and we
					// need to link them inbetween them
					this->alphas[orbit1[d]].values[2] = this->alphas[orbit2[d]].values[2];
					this->alphas[orbit2[d]].values[2] = orbit1[d];
				}
			}
		}
	} else {
		return false;
	}
	return true;
}

// Compute the Euler-Poincare characteristic of the subdivision
int GMap::eulercharacteristic()
{
	return this->elements(2).size() -	// returns the nb of vertices
		this->elements(1).size() +	// returns the nb of edges
		this->elements(0).size();	// returns the nb of faces
}


/*------------------------------------------------------------------------*/

std::pair<std::vector<unsigned short>, std::vector<unsigned int>> GMap3D::get_triangle_indices_and_properties() const {
	std::vector<unsigned short> t_indices;
	std::vector<unsigned int> props;
	// get all faces :
	idlist_t faces = elements(2);
	// for all them faces :
	for (id_t dart : faces) {
		// since they're all squares, we can hardcode the translation mechanism from face to bi-triangle :
		degreelist_t alphas = {0,1};
		idlist_t vpos = orderedorbit(alphas, dart);
		assert(vpos.size() == 8);//get a square
		t_indices.push_back(this->properties.at(get_embedding_dart(vpos[0])));
		t_indices.push_back(this->properties.at(get_embedding_dart(vpos[2])));
		t_indices.push_back(this->properties.at(get_embedding_dart(vpos[4])));
		t_indices.push_back(this->properties.at(get_embedding_dart(vpos[0])));
		t_indices.push_back(this->properties.at(get_embedding_dart(vpos[4])));
		t_indices.push_back(this->properties.at(get_embedding_dart(vpos[6])));
	}
	return std::pair<std::vector<unsigned short>, std::vector<unsigned int>>(t_indices, props);
}

std::pair<std::vector<glm::vec3>,std::vector<glm::vec3>> GMap3D::get_vertices_and_normals() const {
	std::vector<glm::vec3> vertices = this->positions;
	std::vector<glm::vec3> normals;

	std::vector<unsigned short> faces;
	std::vector<unsigned int> props;
	std::tie(faces,props) = this->get_triangle_indices_and_properties();

	for (unsigned int i = 0; i < faces.size(); i+=3) {
		glm::vec3 o = this->positions[faces[i+0]];
		glm::vec3 a = this->positions[faces[i+1]];
		glm::vec3 b = this->positions[faces[i+2]];
		normals.push_back( glm::normalize( glm::cross((a-o), (b-o)) ) );
	}
	return std::pair<std::vector<glm::vec3>,std::vector<glm::vec3>>(vertices,normals);
}

/*------------------------------------------------------------------------*/

GMap3D GMap3D::dual()
{
	return GMap3D(*this);
}


/*------------------------------------------------------------------------*/
