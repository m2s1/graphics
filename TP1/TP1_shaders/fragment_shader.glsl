#version 330 core
in vec4 position;
// Output data
out vec4 FragColor;

void main()
{
	// Output color = red
	FragColor = vec4(position.x,position.y,position.z,1);
}
