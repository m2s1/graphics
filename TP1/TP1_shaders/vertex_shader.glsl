#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
uniform float scaleTriangle;
float x,y,z;
out vec4 position;
//Definir une matrice MVP
uniform mat4 mvpMatrix;
void main(){
	gl_Position =  vec4(vertexPosition_modelspace * scaleTriangle,1) * mvpMatrix;
	position = vec4(gl_Position);
}

