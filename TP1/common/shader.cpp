#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
using namespace std;

#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>

#include "shader.hpp"

GLuint load_shaders(const char * vertex_file_path,const char * fragment_file_path){

	// Create the shaders {{{
	GLuint vShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint programID = glCreateProgram();
	// }}}

	// Read the Vertex Shader code from the file {{{
	std::string vertex_shader_code;
	std::ifstream vertex_shader_stream(vertex_file_path, std::ios::in);
	if(vertex_shader_stream.is_open()){
		std::stringstream sstr;
		sstr << vertex_shader_stream.rdbuf();
		vertex_shader_code = sstr.str();
		vertex_shader_stream.close();
	}else{
		printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
		getchar();
		return 0;
	}
	// }}}

	// Read the Fragment Shader code from the file {{{
	std::string fragment_shader_code;
	std::ifstream fragment_shader_stream(fragment_file_path, std::ios::in);
	if(fragment_shader_stream.is_open()){
		std::stringstream sstr;
		sstr << fragment_shader_stream.rdbuf();
		fragment_shader_code = sstr.str();
		fragment_shader_stream.close();
	}
	// }}}

	GLint result = GL_FALSE;
	int info_log_length;

	char const * vertex_source_pointer = vertex_shader_code.c_str();
	glShaderSource(vShaderID, 1, &vertex_source_pointer, NULL);

	// Compile Vertex Shader
	printf("Compiling and attaching shader : %s ...", vertex_file_path);
	glCompileShader(vShaderID);
	// Check Vertex Shader
	glGetShaderiv(vShaderID, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) {
		printf(" error compiling shader. Will stop now.\n");

		glGetShaderiv(vShaderID, GL_INFO_LOG_LENGTH, &info_log_length);

		GLchar* logMessage = new GLchar[info_log_length];
		glGetShaderInfoLog(vShaderID, info_log_length, NULL, logMessage);

		printf("%s\n", logMessage);

		glDeleteShader(vShaderID);
		exit(EXIT_FAILURE);
	} else {
		printf(" done.\n");
	}

	//*** A completer *************/

	char const * FragmentSourcePointer = fragment_shader_code.c_str();
	glShaderSource(fShaderID, 1, &FragmentSourcePointer, NULL);
	// Compile Fragment Shader
	printf("Compiling and attaching shader : %s ...", fragment_file_path);
	glCompileShader(fShaderID);
	// Check Fragment Shader
	glGetShaderiv(fShaderID, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) {
		printf(" error compiling shader. Will stop now.\n");
		glGetShaderiv(fShaderID, GL_INFO_LOG_LENGTH, &info_log_length);

		GLchar* logMessage = new GLchar[info_log_length];
		glGetShaderInfoLog(fShaderID, info_log_length, NULL, logMessage);

		printf("%s\n", logMessage);

		glDeleteShader(fShaderID);
		exit(EXIT_FAILURE);
	} else {
		printf(" done.\n");
	}

	glAttachShader(programID, vShaderID);
	glAttachShader(programID, fShaderID);

	// Link the program
	printf("Linking program\n");
	glLinkProgram(programID);

	// Check the program
	GLint isLinked = GL_FALSE;
	glGetProgramiv(programID, GL_LINK_STATUS, (int*)&(isLinked));
	if (isLinked == GL_FALSE) {
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &(info_log_length));
		GLchar* logMessage = new GLchar[info_log_length];

		glGetProgramInfoLog(programID, info_log_length, NULL, logMessage);
		printf("%s\n", logMessage);

		glDeleteProgram(programID);
		glDeleteShader(vShaderID);
		glDeleteShader(fShaderID);
	}

	return programID;

	// return 0; //A changer
}


